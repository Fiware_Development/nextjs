import { useContext, useEffect, useState } from "react";
import Allproducts from "../../components/Products/allproducts/allproducts";
import Filterproducts from "../../components/Products/allproducts/filterproducts";
import LangContext from "../../store/LangContext";
import { Container, Row, Col } from "react-bootstrap";
// import classes from '../../styles/filterShopsProducts.module.css'
import Productsads from "../../components/Products/allproducts/productsads";
import Storetitle from "../../components/Products/allproducts/storetitle";
import FilterResponsive from "../../components/Products/allproducts/responsivefilter";

function index() {
  const { lang } = useContext(LangContext);

  const [filterProducts, setFilterProducts] = useState([]);

  const getAllproduct = () => {
    console.log("Get All Products: ", filterProducts);
  };
  const filterProductsArray = (newArray) => {
    setFilterProducts(newArray);
  };
  useEffect(() => {
    getAllproduct();
  }, [filterProducts]);

  return (
    <div>
      <Container fluid>
        <Row>
          <Col lg="3">
            <Filterproducts
              lang={lang}
              getFilteredProducts={filterProductsArray}
            />
            <Productsads lang={lang} />
          </Col>
          <Col lg="9">
            <Col lg="12">
              <Storetitle lang={lang} />
            </Col>
            <Allproducts lang={lang} />
          </Col>
        </Row>
        <FilterResponsive
          lang={lang}
          getFilteredProducts={filterProductsArray}
        />
      </Container>
    </div>
  );
}

export default index;
