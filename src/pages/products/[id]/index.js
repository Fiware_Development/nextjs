import { useContext } from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProductAds from "../../../components/Products/productDetails/ProductAds";
import ProductComDesc from "../../../components/Products/productDetails/ProductComDesc";
import ProductImgs from "../../../components/Products/productDetails/ProductImgs";
import ProductInfo from "../../../components/Products/productDetails/ProductInfo";
import SimilarProducts from "../../../components/Products/productDetails/SimilarProducts";
import LangContext from "../../../store/LangContext";
import storeColAd from "../../../Assets/Images/store/storeColAd.png";
import ProductImgsResponsive from "../../../components/Products/productDetails/ProductImgsResponsive";
import classes from "../../../styles/productDetails.module.css";

const index = () => {
  const { lang } = useContext(LangContext);

  return (
    <Container fluid className={lang === "ar" ? "text-right" : "text-left"}>
      <Row>
        <Col md="9">
          <div>
            <Row>
              <Col lg="8">
                <ProductImgs lang={lang} />
                <ProductImgsResponsive lang={lang} />
              </Col>
              <Col lg="4">
                <ProductInfo lang={lang} />
              </Col>
              <Col lg="12">
                <ProductComDesc lang={lang} />
              </Col>
            </Row>
          </div>
        </Col>
        <Col md="3" className="d-md-block d-lg-block d-none">
          <ProductAds lang={lang} />
        </Col>
        <Col lg="12">
          <div className={classes.storeColAd + " mt-4 mb-5"}>
            <img src={storeColAd} alt="store Ad" className="w-100" />
          </div>
        </Col>
        <Col lg="12">
          <SimilarProducts lang={lang} />
        </Col>
      </Row>
    </Container>
  );
};

export default index;
