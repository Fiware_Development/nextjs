import { useContext } from "react";
import { Container, Row, Col } from "react-bootstrap";
import LangContext from "../../store/LangContext";
import Subscriptionads from "../../components/subscription-representatives/subscriptionrepads";
import Subscriptiontext from "../../components/subscription-representatives/subscriptionreptext";
import Subscriptionvideo from "../../components/subscription-representatives/subscriptionrepvideo";

function index() {
  const { lang } = useContext(LangContext);

  return (
    <Container fluid className={lang === "ar" ? "text-right" : "text-left"}>
      <Row>
        <Col md="9">
          <Subscriptiontext lang={lang} />
          <Subscriptionvideo lang={lang} />
        </Col>

        <Col md="3" className="d-md-block d-lg-block d-none">
          <Subscriptionads lang={lang} />
        </Col>
      </Row>
    </Container>
  );
}

export default index;
