import { useContext } from "react";
import OrderCard from "../../components/mypurchases/orderCard";
import LangContext from "../../store/LangContext";

function index() {
  const { lang } = useContext(LangContext);

  return (
    <div className="w-100">
      <OrderCard lang={lang} />
    </div>
  );
}

export default index;
