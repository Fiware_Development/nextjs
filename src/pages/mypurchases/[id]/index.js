import { useContext } from "react";
import All from "../../../components/mypurchases/purchasesDetails/All";
import Orderinfo from "../../../components/mypurchases/purchasesDetails/orderinfo";
import { Container, Row, Col } from "react-bootstrap";
import LangContext from "../../../store/LangContext";

function index() {
  const { lang } = useContext(LangContext);

  return (
    <div className={lang === "ar" ? "w-100 text-right" : "w-100 text-left"}>
      <Container fluid>
        <Row>
          <Col lg="8">
            <All lang={lang} />
          </Col>
          <Col lg="4">
            <Orderinfo lang={lang} />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default index;
