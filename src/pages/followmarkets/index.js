import { useContext } from "react";
import Followmarkets from "../../components/followMarkets/followmarkets";
import Followmarketsads from "../../components/followMarkets/followmarketsads";
import LangContext from "../../store/LangContext";
import { Container, Row, Col } from "react-bootstrap";

const index = () => {
  const { lang } = useContext(LangContext);

  return (
    <Container fluid className={lang === "ar" ? "text-right" : "text-left"}>
      <Row>
        <Col md="9">
          <Followmarkets lang={lang} />
        </Col>
        <Col md="3" className="d-md-block d-lg-block d-none">
          <Followmarketsads lang={lang} />
        </Col>
      </Row>
    </Container>
  );
};

export default index;
