import React, { useContext } from "react";
import Link from "next/link";

import Fields from "../../components/Fields/Fields";
import { config } from "../../components/LoginConfig/loginconfig";
import { Inputs } from "../../components/LoginConfig/loginInput";
import { Validation } from "../../components/LoginConfig/loginvalidation";
import LangContext from "../../store/LangContext";
import strings from "../../Assets/Local/Local";
import logo from "../../Assets/Images/logo.png";

import FbIcon from "../../components/Icons/FbIcon";
import GoogleIcon from "../../components/Icons/GoogleIcon";

import styles from "../../styles/Login.module.css";
const Login = () => {
  const { lang } = useContext(LangContext);

  const submitHandle = (values) => {
    if (values.rememberMe) {
      cookie.save("tajawalUser", response.data.user, { path: "/" });
      cookie.save("tajawalToken", response.data.token, { path: "/" });
    } else {
      sessionStorage.setItem("tajawalUser", JSON.stringify(response.data.user));
      sessionStorage.setItem(
        "tajawalToken",
        JSON.stringify(response.data.token)
      );
    }
  };
  strings.setLanguage(lang);
  return (
    <div className={`container-fluid ${styles.login_section}`}>
      <div className="row justify-content-center">
        <div className="col-lg-5 col-md-8 col-xs-10">
          <div className={`row ${styles.login_container}`}>
            <div className={`col-12 ${styles.logo_container}`}>
              <img src={logo} alt="tajawal logo" />
            </div>
            <div className="col-12">
              <Fields
                inputs={Inputs}
                config={config}
                submitHandle={submitHandle}
                validation={Validation}
              />
            </div>
            <Link href="/forgetPassword">
              <a className={`col-12 ${styles.forget_link}`}>
                {strings.forgetPassword}
              </a>
            </Link>
            <p className={`col-12 mt-3 text-center`}>{strings.loginVia}</p>
            <div className={`col-4 ${styles.fb_login}`}>
              <FbIcon />
            </div>
            <div className={`col-4 ${styles.go_login}`} role="button">
              <GoogleIcon />
            </div>
            <div
              className="col-12 d-flex justify-content-center mt-2"
              role="button"
            >
              <Link href="/register">
                <a className={`${styles.newRegister}`}>{strings.newRegister}</a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
