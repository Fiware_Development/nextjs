import { useContext } from "react";
import Favtabnav from "../../components/Favourite/favtabnav";
import Favads from "../../components/Favourite/favads";
import LangContext from "../../store/LangContext";
import { Container, Row, Col } from "react-bootstrap";

const index = () => {
  const { lang } = useContext(LangContext);

  return (
    <Container fluid className={lang === "ar" ? "text-right" : "text-left"}>
      <Row>
        <Col md="9">
          <Favtabnav lang={lang} />
        </Col>
        <Col md="3" className="d-md-block d-lg-block d-none">
          <Favads lang={lang} />
        </Col>
      </Row>
    </Container>
  );
};

export default index;
