import { useContext } from "react";
import { Container, Row, Col } from "react-bootstrap";
import productAd from "../../Assets/Images/productAd.png";
import Allshops from "../../components/stores/allshops/Allshops";
import Filtershops from "../../components/stores/allshops/filtershops";
import LangContext from "../../store/LangContext";
import classes from "../../styles/filterShopsProducts.module.css";
import storeAd from "../../Assets/Images/store/storeAd.png";

function index() {
  const { lang } = useContext(LangContext);

  return (
    <div>
      <Container fluid>
        <Row>
          <Col lg="12">
            <div className="">
              <img src={productAd} alt="product ad" className="w-100 my-4" />
            </div>
          </Col>
          <Col lg="12">
            <div className={classes.handleFilter}>
              <i className="fas fa-filter"></i>
            </div>
          </Col>
          <Col lg="3">
            <Filtershops lang={lang} />
            <div
              className={
                lang === "ar"
                  ? "text-right d-lg-block d-none"
                  : "text-left d-lg-block d-none"
              }
            >
              <img src={storeAd} alt="store ad" className="w-100 mt-3" />
            </div>
          </Col>
          <Col lg="9">
            <Allshops lang={lang} />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default index;
