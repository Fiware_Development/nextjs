import { useContext } from "react";
import classes from "../../../styles/storesDetails.module.css";
import { Container, Row, Col } from "react-bootstrap";
import storeColAd from "../../../Assets/Images/store/storeColAd.png";
import StoreSlider from "../../../components/stores/storesDetails/StoreSlider";
import StoreInfo from "../../../components/stores/storesDetails/StoreInfo";
import StoreComments from "../../../components/stores/storesDetails/StoreComments";
import StoreCategories from "../../../components/stores/storesDetails/StoreCategories";
import StoreAds from "../../../components/stores/storesDetails/StoreAds";
import SimilarStores from "../../../components/stores/storesDetails/SimilarStores";
import LangContext from "../../../store/LangContext";

function index() {
  const { lang } = useContext(LangContext);

  return (
    <Container fluid className={lang === "ar" ? "text-right" : "text-left"}>
      <Row>
        <Col md="9">
          <div>
            <Row>
              <Col lg="8">
                <StoreSlider lang={lang} />
              </Col>
              <Col lg="4">
                <StoreInfo lang={lang} />
              </Col>
              <Col lg="12">
                <StoreCategories lang={lang} />
              </Col>
              <Col lg="12">
                <StoreComments lang={lang} />
              </Col>
            </Row>
          </div>
        </Col>
        <Col md="3" className="d-md-block d-lg-block d-none">
          <StoreAds lang={lang} />
        </Col>
        <Col lg="12">
          <div className={classes.storeColAd + " mt-4 mb-5"}>
            <img src={storeColAd} alt="store Ad" className="w-100" />
          </div>
        </Col>
        <Col lg="12">
          <SimilarStores lang={lang} />
        </Col>
      </Row>
    </Container>
  );
}

export default index;
