// const index = () => {
//   return <div>notifications</div>;
// };

// export default index;
import React, { useContext, useState, useRef } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
// import CityCard from "./HomeCards/CityCard";
import LangContext from "../../store/LangContext";
import strings from "../../Assets/Local/Local";
import styles from "../../styles/Notification.module.css";
import pinImg from "../../Assets/Images/store/storeAd.png";
import discount from "../../components/Icons/discount.png";
import order from "../../components/Icons/order.png";
import question from "../../components/Icons/question.png";
import logo from "../../components/Icons/logo.png";
import location from "../../components/Icons/location.png";
import copyIcon from "../../components/Icons/copy.png";
export default function Notifica() {
  const [copySuccess, setCopySuccess] = useState("");

  const { lang } = useContext(LangContext);
  const textAreaRef = useRef(null);

  function copyToClipboard(e) {
    console.log("useRefuseRef:", textAreaRef.current);
    textAreaRef.current.select();
    document.execCommand("copy");
    e.target.focus();
    setCopySuccess("Copied!");
    console.log("copied :", copySuccess, e.target.value);
  }
  const path = {
    parent: { name: { ar: "الرئيسية", en: "Home" }, path: "" },
    children: { name: { ar: "الإشعارات", en: "Notification" }, path: "" },
  };
  const data = [
    {
      date: "22/02/2021 - 05:00 PM",
      title: { ar: "كوبون خصم جديد", en: "" },
      text: "لديك كوبون خصم جديد بنسبة 30% على قيمة الطلب",
      code: "5968QR7",
      type: "OFFER",
    },
    {
      date: "8/02/2021 - 05:00 PM",
      title: { ar: "محلات المجد", en: "" },
      text: "تم إضافة عروض ومنتجات جديدة",
      type: "STORE",
      logo: logo,
    },
    {
      date: "22/02/2021 - 05:00 PM",
      title: { ar: "الإدارة", en: "" },
      text:
        "لوريم إيبسوم طريقة لكتابة النصوص في النشر والتصميم الجرافيكي تستخدم بشكل شائع لتوضيح الشكل المرئي للمستند أو الخط دون الاعتماد على محتوى ذي معنى",
      type: "LOCATION",
    },
    {
      date: "9/02/2021 - 05:00 PM",
      title: { ar: "رقم الطلب : 56455241", en: "" },
      text: "",
      type: "ORDER",
      status: "جاري شحن طلبك",
    },
    {
      date: "22/02/2021 - 05:00 PM",
      title: { ar: "رقم الطلب : 56455241", en: "" },
      text: "",
      type: "ORDER",
      status: "تم وصول طلبك بنجاح",
    },
    {
      date: "15/02/2021 - 05:00 PM",
      title: { ar: "رقم الطلب : 56455241", en: "" },
      text: "",
      type: "ORDER",
      status: "جاري شحن طلبك",
    },
    {
      date: "12/02/2021 - 05:00 PM",
      title: { ar: "رقم الطلب : 56455241", en: "" },
      text: "",
      type: "ORDER",
      status: "جاري شحن طلبك",
    },
    {
      date: "13/02/2021 - 05:00 PM",
      title: { ar: " إسئل المحلات", en: "" },
      text:
        "قامت محلات المجد بالرد على إستفسارك  بخصوص سماعات اذن لاسلكية بميكروفون  ساوند سبورت فري من بوز",
      type: "QUESTION",
    },
  ];

  strings.setLanguage(lang);
  console.log("lang in Notif== > ", lang);

  return (
    <Container fluid>
      <Row className="my-3">
        <Col lg="12">
          <div
            className={
              lang === "ar" ? styles.arPath_container : styles.enPath_container
            }
          >
            <span>{path.parent.name[lang]}</span>
            {lang === "ar" ? ">" : "<"}
            <span>{path.children.name[lang]}</span>
          </div>
        </Col>
      </Row>
      <Row className={styles.main_notification}>
        <Col lg="5">
          {data.map((item, index) => {
            return (
              <Row className="my-3">
                <Col lg="12">
                  <div
                    className={
                      lang === "ar"
                        ? styles.arcontent_container
                        : styles.encontent_container
                    }
                  >
                    <span
                      className={lang === "ar" ? styles.datear : styles.dateen}
                    >
                      {item.date}
                    </span>
                    <Row>
                      <Col lg="2">
                        <div
                          className={
                            lang === "ar" ? styles.iconar : styles.iconen
                          }
                        >
                          {item.type === "OFFER" ? (
                            <img src={discount} />
                          ) : item.type === "ORDER" ? (
                            <img src={order} />
                          ) : item.type === "LOCATION" ? (
                            <img src={location} />
                          ) : item.type === "QUESTION" ? (
                            <img src={question} />
                          ) : (
                            <img src={item.logo} />
                          )}
                        </div>
                      </Col>
                      <Col
                        lg="10"
                        className={
                          lang === "ar" ? styles.titlear : styles.titleen
                        }
                      >
                        <span
                          style={{
                            color:
                              item.type === "OFFER"
                                ? "#F15A24"
                                : item.type === "QUESTION"
                                ? "#F15A24"
                                : "",
                          }}
                        >
                          {item.title[lang]}
                        </span>
                      </Col>
                    </Row>
                    <Row
                      className={lang === "ar" ? styles.textar : styles.texten}
                    >
                      <Col lg="10">
                        <span>{item.text}</span>
                      </Col>
                      <Col
                        lg="2"
                        onClick={copyToClipboard}
                        style={{ cursor: "pointer" }}
                      >
                        {item.type === "OFFER" && (
                          <div className={styles.copyIcon_container}>
                            {" "}
                            <img src={copyIcon} />
                          </div>
                        )}
                      </Col>
                      {item.type === "OFFER" && (
                        <Col lg="10">
                          <span>
                            <textarea
                              ref={textAreaRef}
                              rows={1}
                              className={styles.codetextArea}
                            >
                              {item.code}
                            </textarea>
                          </span>
                        </Col>
                      )}
                    </Row>

                    {item.type === "ORDER" && (
                      <Row
                        className={
                          lang === "ar" ? styles.statusar : styles.statusen
                        }
                      >
                        <Col lg="12">
                          <span> {item.status}</span>
                        </Col>
                        <Col lg="12" className="my-2">
                          <Button className={styles.details_btn}>
                            تفاصيل الطلب
                          </Button>
                        </Col>
                      </Row>
                    )}
                    {item.type === "QUESTION" && (
                      <Row
                        className={
                          lang === "ar" ? styles.statusar : styles.statusen
                        }
                      >
                        <Col lg="12" className="my-2">
                          <Button className={styles.details_btn}>
                            {" "}
                            تواصل مع المتجر
                          </Button>
                        </Col>
                      </Row>
                    )}
                  </div>
                </Col>
              </Row>
            );
          })}
        </Col>
        <Col lg="3">
          <div>
            <img src={pinImg} />
          </div>
        </Col>
      </Row>
    </Container>
  );
}
