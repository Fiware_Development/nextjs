// const index = () => {
//   return <div>notifications</div>;
// };

// export default index;
import React, { useContext,useState,useRef } from "react";
import { Container, Row, Col} from "react-bootstrap";
// import CityCard from "./HomeCards/CityCard";
import LangContext from "../../store/LangContext";
import strings from "../../Assets/Local/Local";
import styles from "../../styles/Notification.module.css";
import pinImg from "../../Assets/Images/store/storeAd.png";
import logo from './Icons/logo.png';
import Link from 'next/link'
export default function chat() {

  const { lang } = useContext(LangContext);

  const path = {
    parent: { name: { ar: "الرئيسية", en: "Home" }, path: '' },
    children: { name: { ar: "المحادثات", en: "Chat" }, path: '' }
  };
  const data = [
    { date: '8/02/2021 - 05:00 PM', count:"2",title: { ar: 'محلات المجد', en: '' }, text: 'تم إضافة عروض ومنتجات جديدة', logo: logo },
    { date: '22/02/2021 - 05:00 PM', count:"5",title: { ar: 'محلات ألعويس', en: '' }, text: 'قامت محلات المجد بالرد على إستفسارك  بخصوص سماعات اذن لاسلكية بميكروفون  ساوند سبورت فري من ',  logo: logo },
    { date: '18/02/2021 - 05:00 PM',count:"1", title: { ar: 'محلات العويس', en: '' }, text: 'قامت محلات المجد بالرد على إستفسارك  بخصوص سماعات اذن لاسلكية بميكروفون  ساوند سبورت فري من ', logo: logo },
    { date: '8/02/2021 - 05:00 PM', count:"0",title: { ar: 'محلات العويس', en: '' }, text: 'تم إضافة عروض ومنتجات جديدة',  logo: logo },
    { date: '28/02/2021 - 05:00 PM', count:"0",title: { ar: 'محلات العويس', en: '' }, text: 'قامت محلات المجد بالرد على إستفسارك  بخصوص سماعات اذن لاسلكية بميكروفون  ساوند سبورت فري من ', logo: logo },
 
]

  strings.setLanguage(lang);

  return (
    <Container fluid >
      <Row className="my-3">
        <Col lg="12" >
          <div
            className={lang === "ar" ? styles.arPath_container : styles.enPath_container}
          >
            <span>{path.parent.name[lang]}</span>
            {lang === "ar" ? ">" : "<"}
            <span>{path.children.name[lang]}</span>
          </div>
        </Col>
      </Row>
      <Row className={styles.main_notification}>
        <Col lg="5" >
          {
            data.map((item, index) => {
              return (
                <Row className="my-3" style={{cursor:"pointer"}} >
                     <Link href="/chat/chat" state={{
                       name:item.title
                     }}>
                  <Col lg="12" >
                    <div className={lang === "ar" ? styles.arcontent_container : styles.encontent_container}>
                      <span className={lang === "ar" ? styles.datear : styles.dateen}>{item.date}</span>
                      <Row>
                        <Col lg="2">
                          <div className={lang === "ar" ? styles.iconar : styles.iconen}>
                                    <img src={item.logo} />
                          </div>

                        </Col>
                        <Col lg="10" className={lang === "ar" ? styles.titlear : styles.titleen}>
                          <span className={lang === "ar" ?"ml-3 pt-2":"mr-3 pt-2"} >{item.title[lang]}</span>
                          {item.count>0&&<span className={styles.count_container}>
                             {item.count}
                          </span>}
                        </Col>
                      </Row>
                      <Row className={lang === "ar" ? styles.textar : styles.texten}>
                        <Col lg="10">
                          <span>{item.text}</span>
                        </Col>
                      </Row>
                       
                     

                    </div>

                  </Col>
                  </Link>
                </Row>
              )
            })
          }
        </Col>
        <Col lg="3" >
          <div>
            <img src={pinImg} />
          </div>
        </Col>
      </Row>
    </Container>
  );
}
