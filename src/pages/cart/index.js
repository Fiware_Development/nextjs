import React, { useState, useContext } from "react";
import Link from "next/link";
import strings from "../../Assets/Local/Local";
import LangCtx from "../../store/LangContext";

import productImg1 from "../../Assets/Images/prodImg1.png";
import productImg2 from "../../Assets/Images/product3.png";
import productImg3 from "../../Assets/Images/product4.png";
import adsImg1 from "../../Assets/Images/ads3.png";
import adsImg2 from "../../Assets/Images/ads4.png";
import styles from "../../styles/shoppingCart.module.css";
import ShoppingCart from "../../components/Cards/ShoppingCart";
const Cart = () => {
  const { lang } = useContext(LangCtx);
  const [cart, setCart] = useState([
    {
      markerName: "محلات العويس",
      deliveryCost: 100,
      products: [
        {
          id: 1,
          productName: `سماعة رأس ايربودز ماكس بخاصية
إلغاء الضوضاء النشط من ابل`,
          price: 2000,
          status: "جديدة",
          seller: "محلات العويس",
          image: productImg1,
          count: 1,
        },
        {
          id: 2,
          productName: `ساعة سمارت ماجيك واتش 2 من
هونر HBE-B19، 42 ملم`,
          price: 1500,
          status: "جديدة",
          seller: "محلات العويس",
          image: productImg2,
          count: 1,
        },
      ],
    },
    {
      markerName: "أسواق المحد",
      deliveryCost: 50,
      products: [
        {
          id: 3,
          productName: `سماعات اذن لاسلكية بميكروفون
ساوند سبورت فري من بوز`,
          price: 2000,
          status: "جديدة",
          seller: "أسواق المحد",
          image: productImg3,
          count: 1,
        },
      ],
    },
  ]);
  const totalProducts = cart.reduce(
    (accumaltor, item) => accumaltor + item.products.length,
    0
  );

  const deleteProduct = (id) => {};
  const countHandel = (id, method, index) => {};
  strings.setLanguage(lang);
  return (
    <div className={`container-fluid ${styles.shopping_cart}`}>
      <div className="row">
        {cart.length ? (
          <>
            <div className="col-12">
              <p>Path</p>
            </div>
            <div className={`col-lg-6 col-md-9`}>
              {cart.map((item, idx) => (
                <section className={styles.market_item_container} key={idx}>
                  <div className={styles.market_name}>
                    <h6>
                      <span>{strings.store}</span> : {item.markerName}
                    </h6>
                    <p>{`${strings.priceDelivery} : ${item.deliveryCost} ${strings.currency}`}</p>
                  </div>
                  {item.products.map((product, index) => (
                    <ShoppingCart
                      product={product}
                      deleteProductHandel={deleteProduct}
                      key={index}
                      countHandel={countHandel}
                    />
                  ))}
                </section>
              ))}
            </div>
            <div className={`col-lg-4 col-md-3`}>
              <div className={styles.products_sum}>
                <p>
                  {strings.numOfProducts} : {totalProducts}{" "}
                  {totalProducts > 1 ? strings.Products : strings.product}
                </p>
                <p>
                  {strings.priceDelivery} : 150 {strings.currency}{" "}
                </p>
              </div>
              <div className={styles.cart_total}>
                <p>{strings.total}</p>
                <h6>6000 {strings.currency}</h6>
                <small>{strings.taxValue}</small>
              </div>
              <Link href="/">
                <a className={styles.btn_checkout}>{strings.btncheckout}</a>
              </Link>
            </div>
            <div className={`col-lg-2 col-md-3 ${styles.ads_container}`}>
              <div className={styles.img_container}>
                <img src={adsImg1} alt="ads" />
              </div>
              <div className={styles.img_container}>
                <img src={adsImg2} alt="ads" />
              </div>
            </div>
          </>
        ) : null}
      </div>
    </div>
  );
};

export default Cart;
