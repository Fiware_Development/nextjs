import classes from '../../../styles/productDetails.module.css'
import strings from "../../../Assets/Local/Local";

function SimilarProducts(props) {

  strings.setLanguage(props.lang);

  return (
    <div className={classes.similarProducts}>
      <h2>{strings.similarProducts}</h2>
    </div>
  )
}

export default SimilarProducts
