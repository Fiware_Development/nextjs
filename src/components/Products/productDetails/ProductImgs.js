import { useState } from 'react'
import classes from '../../../styles/productDetails.module.css'
import prodImg2 from '../../../Assets/Images/prodImg2.png'
import prodImg1 from '../../../Assets/Images/prodImg1.png'
import adds1 from '../../../Assets/Images/adds1.png'
import adds2 from '../../../Assets/Images/adds2.png'

function ProductImgs() {
  const [imgSrc, setImgSrc] = useState(prodImg1)

  const handleImgIndex = (imgSrc) => {
    setImgSrc(imgSrc)
  }

  return (
    <div className={classes.prodImgs + ' d-lg-flex d-none'}>
      <div className={classes.prodSmallImg}>
        <div
          className="d-flex justify-content-center align-items-center"
          onClick={() => handleImgIndex(prodImg2)}
        >
          <img src={prodImg2} alt="product img" className="img-fluid" />
        </div>
        <div
          className="d-flex justify-content-center align-items-center"
          onClick={() => handleImgIndex(adds1)}
        >
          <img src={adds1} alt="product img" className="img-fluid" />
        </div>
        <div
          className="d-flex justify-content-center align-items-center"
          onClick={() => handleImgIndex(adds2)}
        >
          <img src={adds2} alt="product img" className="img-fluid" />
        </div>
        <div
          className="d-flex justify-content-center align-items-center"
          onClick={() => handleImgIndex(prodImg2)}
        >
          <img src={prodImg2} alt="product img" className="img-fluid" />
        </div>
        <div
          className="d-flex justify-content-center align-items-center"
          onClick={() => handleImgIndex(adds2)}
        >
          <img src={adds2} alt="product img" className="img-fluid" />
        </div>
      </div>
      <div
        className={
          classes.prodFullImg +
          ' d-flex justify-content-center align-items-center'
        }
      >
        <img src={imgSrc} alt="product img" className="img-fluid" />
      </div>
    </div>
  )
}

export default ProductImgs
