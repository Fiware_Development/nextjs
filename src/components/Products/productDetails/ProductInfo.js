import { Button } from 'react-bootstrap'
import classes from '../../../styles/productDetails.module.css'
import ReactStars from 'react-rating-stars-component'
import strings from "../../../Assets/Local/Local";

function ProductInfo(props) {

  strings.setLanguage(props.lang);

  const ratingChanged = (newRating) => {
  }

  return (
    <div className={classes.prodInfo}>
      <h4 className={classes.prodName}>
        سماعة رأس ايربودز ماكس بخاصية إلغاء الضوضاء النشط من ابل
      </h4>
      <span className={classes.prodRate}>
        <i className="fas fa-star"></i> 4.5
      </span>
      <p className={classes.prodPrice}>
        2.000 <span className="mx-2">{strings.currency}</span>
      </p>
      <p className={classes.prodTax}>{strings.priceVAT}</p>
      <div
        className={classes.prodStatus + ' border-top border-bottom py-3 my-3'}
      >
        <h6>{strings.productState}: جديدة</h6>
        <h6>
          {strings.theSeller}: <span>محلات العويس</span>
        </h6>
      </div>
      <div className={classes.adFavCart + ' d-flex border-bottom pb-3 mb-3'}>
        <Button className="px-3 px-xl-4 py-2">{strings.addToCartBTN}</Button>
        <Button className={classes.prodFav + ' px-3 py-2 mx-2 border rounded'}>
          <i className="fas fa-heart"></i>
        </Button>
      </div>
      <div
        className={
          classes.prodAdRate +
          ' bg-white py-3 px-4 d-flex justify-content-between align-items-center shadow-sm'
        }
      >
        <span>{strings.addYourRate}</span>
        <div className={classes.rating}>
          <ReactStars
            count={5}
            onChange={ratingChanged}
            size={22}
            activeColor="#F15A24"
            value="2"
          />
        </div>
      </div>
    </div>
  )
}

export default ProductInfo
