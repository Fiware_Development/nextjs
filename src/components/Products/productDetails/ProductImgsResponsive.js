import prodImg2 from '../../../Assets/Images/prodImg2.png'
import prodImg1 from '../../../Assets/Images/prodImg1.png'
import adds1 from '../../../Assets/Images/adds1.png'
import adds2 from '../../../Assets/Images/adds2.png'
import Slider from "react-slick";

function ProductImgsResponsive() {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        // autoplay: true,
    };

    return (
        <div className='prodImgsResponsive d-lg-none d-block'>
            <Slider {...settings}>
                <div className="prodImgsResponsiveImages">
                    <img src={prodImg2} alt="product img" className="w-100 h-100" />
                </div>
                <div className="prodImgsResponsiveImages">
                    <img src={prodImg1} alt="product img" className="w-100 h-100" />
                </div>
                <div className="prodImgsResponsiveImages">
                    <img src={adds1} alt="product img" className="w-100 h-100" />
                </div>
                <div className="prodImgsResponsiveImages">
                    <img src={adds2} alt="product img" className="w-100 h-100" />
                </div>
            </Slider>
        </div>
    )
}

export default ProductImgsResponsive
