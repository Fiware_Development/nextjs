import { useState } from 'react'
import classes from '../../../styles/filterShopsProducts.module.css'
import strings from '../../../Assets/Local/Local'
import { Button, Form, Modal, Tabs, Tab } from 'react-bootstrap'

function FilterResponsive(props) {

    const [ordering, setOrdering] = useState([])
    const [categories, setCategories] = useState([])

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleFilterOrdering = (e) => {
        let newArrayFilter = [...ordering, e.target.id]
        if (ordering.includes(e.target.id)) {
            newArrayFilter = newArrayFilter.filter(activity => activity !== e.target.id);
        }
        setOrdering(newArrayFilter)
        props.getFilteredProducts(newArrayFilter)
    }
    const handleFilterCategories = (e) => {
        let newArrayFilter = [...categories, e.target.id]
        if (categories.includes(e.target.id)) {
            newArrayFilter = newArrayFilter.filter(category => category !== e.target.id);
        }
        setCategories(newArrayFilter)
        props.getFilteredProducts(newArrayFilter)
    }

    strings.setLanguage(props.lang)
    return (
        <div className="d-lg-none d-block">

            <div className={classes.handleFilter} onClick={handleShow}>
                <i className="fas fa-filter"></i>
            </div>

            <Modal
                className="responsiveFilter"
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton className="border-0 pb-0">
                </Modal.Header>
                <Modal.Body>
                    <Tabs defaultActiveKey="categories" id="uncontrolled-tab-example" className='mb-3 border-0'>
                        <Tab eventKey="categories" title={strings.categories} className={props.lang === 'ar' ? "text-right" : "text-left"}>
                            <Form.Group controlId="c0" className={classes.filterFormGroup + ` ${props.lang === 'ar' ? "responsiveFormGroup" : ""}`}>
                                <Form.Check type="checkbox" label={strings.all} onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="c1" className={classes.filterFormGroup + ` ${props.lang === 'ar' ? "responsiveFormGroup" : ""}`}>
                                <Form.Check type="checkbox" label="c1" onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="c2" className={classes.filterFormGroup + ` ${props.lang === 'ar' ? "responsiveFormGroup" : ""}`}>
                                <Form.Check type="checkbox" label="c2" onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="c3" className={classes.filterFormGroup + ` ${props.lang === 'ar' ? "responsiveFormGroup" : ""}`}>
                                <Form.Check type="checkbox" label="c3" onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                        </Tab>
                        <Tab eventKey="order" title={strings.orderBy} className={props.lang === 'ar' ? "text-right" : "text-left"}>
                            <Form.Group controlId="a0" className={classes.filterFormGroup + ` ${props.lang === 'ar' ? "responsiveFormGroup" : ""}`}>
                                <Form.Check type="checkbox" label={strings.all} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a1" className={classes.filterFormGroup + ` ${props.lang === 'ar' ? "responsiveFormGroup" : ""}`}>
                                <Form.Check type="checkbox" label={strings.lessPrice} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a2" className={classes.filterFormGroup + ` ${props.lang === 'ar' ? "responsiveFormGroup" : ""}`}>
                                <Form.Check type="checkbox" label={strings.highPrice} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a3" className={classes.filterFormGroup + ` ${props.lang === 'ar' ? "responsiveFormGroup" : ""}`}>
                                <Form.Check type="checkbox" label={strings.highRate} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a3" className={classes.filterFormGroup + ` ${props.lang === 'ar' ? "responsiveFormGroup" : ""}`}>
                                <Form.Check type="checkbox" label={strings.addedNewly} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                        </Tab>
                    </Tabs>
                </Modal.Body>
            </Modal>

        </div>
    )
}

export default FilterResponsive
