import { useState } from 'react'
import Productcard from '../../../components/Cards/productcard'
import { Row, Col } from 'react-bootstrap'
import productAd from '../../../Assets/Images/productAd.png'

function Allproducts(props) {

    const [allProducts, setAllProducts] = useState([
        {
            id: 1,
            name: 'product one',
            rate: 2.5,
            price: 200,
        },
        {
            id: 2,
            name: 'product two',
            rate: 4.0,
            price: 250,
        },
        {
            id: 3,
            name: 'product three',
            rate: 2.5,
            price: 200,
        },
        {
            id: 4,
            name: 'product four',
            rate: 2.5,
            price: 200,
        },
        {
            id: 5,
            name: 'product five',
            rate: 2.5,
            price: 200,
        },
        {
            id: 6,
            name: 'product six',
            rate: 2.5,
            price: 200,
        },
    ])
    function addFavoriteHandler(prodId) {
        console.log("Add Succes", prodId)
    }
    function removeFavoriteHandler(prodId) {
        console.log("Removed Succes", prodId)
    }

    return (
        <Row>
            {allProducts.map((prod) => {
                return <Col xl="3" lg="4" md="4" sm="6" key={prod.id}>
                    <Productcard lang={props.lang} prodData={prod}
                        removeProductFromFavorite={removeFavoriteHandler}
                        addProductToFavorite={addFavoriteHandler} />
                </Col>
            })}

            <Col lg="12">
                <div className="">
                    <img src={productAd} alt="product ad" className="w-100 my-4" />
                </div>
            </Col>
        </Row>
    )
}

export default Allproducts
