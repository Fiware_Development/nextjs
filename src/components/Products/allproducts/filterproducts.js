import { useState } from 'react'
import classes from '../../../styles/filterShopsProducts.module.css'
import { Accordion, Card, Button, Form } from 'react-bootstrap'
import filterBTN from '../../../Assets/Images/filterBTN.png'
import strings from '../../../Assets/Local/Local'

function Filterproducts(props) {
    const [ordering, setOrdering] = useState([])
    const [categories, setCategories] = useState([])

    const handleFilterOrdering = (e) => {
        let newArrayFilter = [...ordering, e.target.id]
        if (ordering.includes(e.target.id)) {
            newArrayFilter = newArrayFilter.filter(activity => activity !== e.target.id);
        }
        setOrdering(newArrayFilter)
        props.getFilteredProducts(newArrayFilter)
    }
    const handleFilterCategories = (e) => {
        let newArrayFilter = [...categories, e.target.id]
        if (categories.includes(e.target.id)) {
            newArrayFilter = newArrayFilter.filter(category => category !== e.target.id);
        }
        setCategories(newArrayFilter)
        props.getFilteredProducts(newArrayFilter)
    }

    strings.setLanguage(props.lang)

    return (
        <div className={classes.filterBody + ` ${props.lang === 'ar' ? 'text-right' : 'text-left'}`}>
            <Accordion defaultActiveKey="0">
                <Card className={classes.filterCard}>
                    <Card.Header className={classes.filterCardHeader}>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0" className={classes.filterBTN}>
                            <span><img src={filterBTN} alt="filter BTN" className="img-fluid" /></span> <span className="mx-2">{strings.categories}</span>
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body className={classes.filterCardBody}>
                            <Form.Group controlId="c0" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label={strings.all} onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="c1" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="c1" onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="c2" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="c2" onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="c3" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="c3" onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
            <Accordion defaultActiveKey="0">
                <Card className={classes.filterCard}>
                    <Card.Header className={classes.filterCardHeader}>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0" className={classes.filterBTN}>
                            <span><img src={filterBTN} alt="filter BTN" className="img-fluid" /></span> <span className="mx-2">{strings.orderBy}</span>
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body className={classes.filterCardBody}>
                            <Form.Group controlId="a0" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label={strings.all} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a1" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label={strings.lessPrice} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a2" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label={strings.highPrice} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a3" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label={strings.highRate} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a3" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label={strings.addedNewly} onChange={handleFilterOrdering} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    )
}

export default Filterproducts
