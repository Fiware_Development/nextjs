import { useContext } from "react";
import MetaDecrator from "../MetaDecrator/MetaDecrator";
import Layout from "../Layout/Layout";
import LoginFooter from "../LoginFooter/LoginFooter";
import LangContext from "../../store/LangContext";
import UserContext from "../../store/UserContext";
const MainTheme = ({ Component, pageProps }) => {
  const { lang } = useContext(LangContext);
  const { user } = useContext(UserContext);
  console.log("lang in header=>", lang);
  return (
    <>
      <MetaDecrator />
      <div dir={lang === "ar" ? "rtl" : "ltr"}>
      <Layout>
            <Component {...pageProps} />
          </Layout>
        {/* {Object.keys(user).length ? (
          <Layout>
            <Component {...pageProps} />
          </Layout>
        ) : (
          <>
            <Component {...pageProps} />
            <LoginFooter />
          </>
        )} */}
      </div>
    </>
  );
};

export default MainTheme;
