import Head from "next/head";
const metaDecartor = ({ title, description, imgUrl, imgAlt }) => {
  return (
    <Head>
      <meta charset="UTF-8" />
      <meta name="description" content={description} />
      <meta name="keywords" content="تجول - شراء منتجات - شراء اونلاين " />
      <meta name="author" content="Tajwal" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0"
      ></meta>
      <meta property="og:title" content={title} />
      <meta property="og:description" content={{ description }} />
      <meta property="og:image" content={imgUrl} />

      <meta name="twitter:card" content={imgUrl} />

      <meta property="og:site_name" content={title} />
      <meta name="twitter:image:alt" content={imgAlt}></meta>
      <title>{title}</title>
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
        crossOrigin="anonymous"
      />
    </Head>
  );
};
metaDecartor.defaultProps = {
  title: "Tajwal",
  description:
    " يقوم المستخدم بالتجول بالاسواق كما لو انه خرج ليتسوق بنفسه  دون الحاجة الي  تسجيل الدخول من خلال اليه البحث يستطيع المستخدم ان يشاهد جميع التفاصيل والمعلومات عن المتجر(الموقع – التقيم – التعليقات – صور للمتجر – مواعيد العمل ) ويستطيع المستخدم ان يتابع المتاجر لكي يصل له كل جديد",
  imgUrl: "https://www.mediafire.com/convkey/99d4/ljdmmqkko5yl5iqzg.jpg",
  imgAlt: "Tajwal logo",
};
export default metaDecartor;
