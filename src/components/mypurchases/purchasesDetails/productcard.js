import classes from '../../../styles/mypurchasesDetails.module.css'
import strings from '../../../Assets/Local/Local'
import storeLogo from "../../../Assets/Images/store/storeLogo.png";

function Productcard(props) {

    strings.setLanguage(props.lang)

    return (
        <div className={classes.orderProductCard + ' bg-white p-3 d-flex rounded mb-3'}>
            <div className={classes.orderProductCardImg}>
                <img src={storeLogo} alt="product img" className="img-fluid" />
            </div>
            <div className={classes.orderProductCardInfo + ' mx-3'}>
                <h5 style={{ color: "#061637" }} className="text-truncate">سماعة رأس ايربودز ماكس بخاصية سماعة رأس ايربودز ماكس بخاصية</h5>
                <p style={{ color: "#F15A24" }} className="mb-2">800 <span className="mx-2">{strings.currency}</span></p>
                <p style={{ color: "#626D81" }} className="mb-0">{strings.amount} : 1</p>
            </div>
        </div>
    )
}

export default Productcard
