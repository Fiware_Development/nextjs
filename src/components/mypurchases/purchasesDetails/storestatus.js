import classes from '../../../styles/mypurchasesDetails.module.css'
import styles from '../../../styles/mypurchases.module.css'
import strings from '../../../Assets/Local/Local'

function Storestatus(props) {

    strings.setLanguage(props.lang)

    return (
        <div>
            <div className={classes.storeName + ' p-2'}>
                <p className="mb-0" style={{color: "#9EA4AF"}}><span>{strings.store}</span> : <span style={{ color: "#F15A24" }}>محلات العويس</span></p>
                <div className="d-flex">
                    <div className={classes.chat + ' text-white rounded p-2 mx-2 d-flex justify-content-center align-items-center'}><i className="fas fa-comment-alt"></i></div>
                    <h6 className={styles.cancelOrderBTN + ' rounded text-white p-2 text-center mb-0 d-flex justify-content-center align-items-center'}>{strings.cancelOrder}</h6>
                </div>
            </div>
            <div className={styles.orderState + ' my-3'}>
                <div className={styles.chargingState}>
                    <div className={styles.orderStateIcon}><i className="fas fa-check"></i></div>
                    <div className={styles.orderStateIcon}><i className="fas fa-check"></i></div>
                    <div className={styles.orderStateIcon}><i className="fas fa-check"></i></div>
                </div>
                <div className={styles.chargingState + ' mt-3'}>
                    <div className={styles.orderStateText}>{strings.waitingState}</div>
                    <div className={styles.orderStateText}>{strings.chargingState}</div>
                    <div className={styles.orderStateText}>{strings.deliveredState}</div>
                </div>
            </div>
        </div>
    )
}

export default Storestatus
