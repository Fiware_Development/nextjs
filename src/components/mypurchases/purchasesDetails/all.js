import Storestatus from './storestatus'
import Productcard from './productcard'
import { Row, Col } from 'react-bootstrap'

function All(props) {
    return (
        <Row>
            <Col md="6">
                <Storestatus lang={props.lang} />
                <Productcard lang={props.lang} />
                <Productcard lang={props.lang} />
                <Productcard lang={props.lang} />
            </Col>
            <Col md="6">
                <Storestatus lang={props.lang} />
                <Productcard lang={props.lang} />
                <Productcard lang={props.lang} />
                <Productcard lang={props.lang} />
            </Col>
        </Row>
    )
}

export default All
