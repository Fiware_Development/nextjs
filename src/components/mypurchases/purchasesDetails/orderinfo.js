import classes from '../../../styles/mypurchasesDetails.module.css'
import strings from '../../../Assets/Local/Local'
import cashIcon from '../../../Assets/Images/cashIcon.png'
import Moment from 'react-moment';

function Orderinfo(props) {

    strings.setLanguage(props.lang)

    return (
        <div className={classes.orderInfo + ' mt-lg-0 mt-4'}>
            <div className={classes.orderDetails + ' bg-white mb-3 p-3 rounded'}>
                <div>
                    <h6 style={{ color: "#626D81" }}>{strings.orderNumber} : 56455241</h6>
                    <h6 style={{ color: "#9EA4AF" }}>
                        <Moment format="hh:mm - DD/MM/YYYY">1976-04-19T12:59-0500</Moment>
                    </h6>
                    <h6 style={{ color: "#626D81" }}>{strings.numOfStores} : 3 {strings.storeOrder}</h6>
                    <h6 style={{ color: "#626D81" }}>{strings.numOfProducts} : 3 {strings.productsOrder}</h6>

                    <h6 style={{ color: "#626D81" }}>{strings.priceDelivery} : 3 {strings.currency}</h6>
                    <h6 style={{ color: "#061637" }}>{strings.total} : 3 {strings.currency}</h6>
                    <h6 style={{ color: "#F15A24" }}>{strings.discountPercentage} : 3 <span className="mx-3">%</span></h6>
                </div>
                <div>
                    <h6 className={classes.cancelOrderBTN + ' text-white py-2 text-center my-4'}>{strings.cancelOrder}</h6>
                </div>
            </div>
            <div className={classes.totalAmount + ' bg-white mb-3 p-3 rounded'}>
                <h6 style={{ color: "#061637" }}>{strings.totalAmount}</h6>
                <p className={classes.price + " mb-2"} style={{ color: "#F15A24" }}> 6.000 <span className="mx-2">{strings.currency}</span> </p>
                <p className="mb-0" style={{ color: "#9EA4AF" }}>{strings.priceInVAT}</p>
            </div>
            <h6 className="mb-3" >{strings.paymentWay}</h6>
            <div className={classes.paymentWay + ' bg-white p-3 rounded'}>
                <p className="mb-0" style={{ color: "#061637" }}> <img src={cashIcon} alt="cashIcon" className="img-fluid" /> <span className="mx-2">{strings.cashOnDelivery}</span> </p>
            </div>
        </div>
    )
}

export default Orderinfo
