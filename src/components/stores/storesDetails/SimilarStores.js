import classes from '../../../styles/storesDetails.module.css'
import strings from "../../../Assets/Local/Local";

function SimilarStores(props) {

  strings.setLanguage(props.lang);

  return (
    <div className={classes.similarStores}>
      <h2>{strings.similarShops}</h2>
    </div>
  )
}

export default SimilarStores
