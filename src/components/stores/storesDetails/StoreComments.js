import userCommentImg from '../../../Assets/Images/store/userCommentImg.png'
import classes from '../../../styles/storesDetails.module.css'
import titleImg from '../../../Assets/Images/store/titleImg.png'
import { InputGroup, FormControl } from 'react-bootstrap'
import ReactStars from 'react-rating-stars-component'
import strings from "../../../Assets/Local/Local";

function StoreComments(props) {

  strings.setLanguage(props.lang);

  const ratingChanged = (newRating) => {
  }

  return (
    <div className="mt-5">
      <h4
        className={
          `${props.lang === 'ar'
            ? classes.storeDivTitleAR
            : classes.storeDivTitleEN
          }` + ' mb-4 position-relative d-flex align-items-center'
        }
      >
        <img src={titleImg} alt="title img" className="position-absolute" />
        <span>{strings.commentsAndRates}</span>
      </h4>
      <div className={classes.userComment + ' bg-white mb-3 py-3 px-4'}>
        <div className="d-flex justify-content-between">
          <div className="d-flex align-items-center">
            <div className={classes.userImg}>
              <img src={userCommentImg} alt="user img" className="img-fluid" />
            </div>
            <div
              className={
                classes.userName + ` ${props.lang === 'ar' ? ' mr-2' : ' ml-2'}`
              }
            >
              <span className="d-block">احمد خالد</span>
              <span className="text-muted">{strings.since} 3 {strings.hours}</span>
            </div>
          </div>
          <div className={classes.rating}>
            <ReactStars
              count={5}
              onChange={ratingChanged}
              size={22}
              activeColor="#F15A24"
              value="4"
            />
          </div>
        </div>
        <p className="mt-3">
          هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي
          القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة
          التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ
          طبيعياَ هناك حقيقة مثبتة
        </p>
      </div>
      <div className={classes.userComment + ' bg-white mb-3 py-3 px-4'}>
        <div className="d-flex justify-content-between">
          <div className="d-flex">
            <div className={classes.userImg}>
              {' '}
              <img src={userCommentImg} alt="user img" className="img-fluid" />
            </div>
            <div
              className={
                classes.userName + ` ${props.lang === 'ar' ? ' mr-2' : ' ml-2'}`
              }
            >
              <span className="d-block">احمد خالد</span>
              <span className="text-muted">منذ 3 ساعات</span>
            </div>
          </div>
          <div className={classes.rating}>
            <ReactStars
              count={5}
              onChange={ratingChanged}
              size={22}
              activeColor="#F15A24"
              value="4"
            />
          </div>
        </div>
        <p className="mt-3">
          هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي
          القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة
          التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ
          طبيعياَ هناك حقيقة مثبتة
        </p>
      </div>
      <div
        className={
          classes.showAllComments + ' d-flex mx-auto mb-3 py-2 px-4 my-4'
        }
      >
        <span>{strings.showAllComments}</span>{' '}
        <i
          className={
            props.lang === 'ar'
              ? 'fas fa-sort-down mr-3'
              : 'fas fa-sort-down ml-3'
          }
        ></i>
      </div>
      <div>
        <InputGroup className={classes.addCommentInput}>
          <FormControl
            placeholder={strings.addYourComment}
            aria-label="Username"
            aria-describedby="basic-addon1"
          />
          <InputGroup.Prepend>
            <InputGroup.Text
              className={
                classes.addCommentBTN +
                ' text-white d-flex justify-content-center shadow-sm'
              }
              id="basic-addon1"
            >
              {strings.sendBTN}
            </InputGroup.Text>
          </InputGroup.Prepend>
        </InputGroup>
      </div>
    </div>
  )
}

export default StoreComments
