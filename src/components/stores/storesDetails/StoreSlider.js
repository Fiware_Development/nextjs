import adds1 from '../../../Assets/Images/adds1.png'
import adds2 from '../../../Assets/Images/adds2.png'
import Slider from "react-slick";

function StoreSlider(props) {

  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 2000,
    appendDots: dots => (
      <div
      >
        <ul className="mb-0"> {dots} </ul>
      </div>
    ),
    customPaging: i => (
      <div>
        {i + 1}
      </div>
    )
  };

  return (
    <div className={props.lang === "ar" ? "storeSlider storeSliderAR" : "storeSlider storeSliderEN"}>
      <Slider {...settings}>
        <div>
          <img src={adds1} alt="img" className="w-100 h-100" />
        </div>
        <div>
          <img src={adds2} alt="img" className="w-100 h-100" />
        </div>
        <div>
          <img src={adds2} alt="img" className="w-100 h-100" />
        </div>
        <div>
          <img src={adds1} alt="img" className="w-100 h-100" />
        </div>
      </Slider>
    </div>
  )
}

export default StoreSlider
