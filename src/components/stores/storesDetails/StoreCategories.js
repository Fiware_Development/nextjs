import titleImg from "../../../Assets/Images/store/titleImg.png";
import classes from "../../../styles/storesDetails.module.css";
import Link from "next/link";
import strings from "../../../Assets/Local/Local";

function StoreCategories(props) {
  strings.setLanguage(props.lang);

  return (
    <div className={classes.storeCategories + " mt-5"}>
      <h4
        className={
          `${
            props.lang === "ar"
              ? classes.storeDivTitleAR
              : classes.storeDivTitleEN
          }` + " mb-3 position-relative d-flex align-items-center"
        }
      >
        <img src={titleImg} alt="title img" className="position-absolute" />
        <span>{strings.categories}</span>
      </h4>
      <Link href="/">السماعات</Link>
      <Link href="/">السماعات</Link>
      <Link href="/">السماعات</Link>
      <Link href="/">السماعات</Link>
      <Link href="/">السماعات</Link>
      <div
        className={classes.showAllProducts + " text-center rounded-lg mt-4"}
      >
        <Link href="/products">{strings.showAllProducts}</Link>
      </div>
    </div>
  );
}

export default StoreCategories;
