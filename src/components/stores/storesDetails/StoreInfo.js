import storeLogo from "../../../Assets/Images/store/storeLogo.png";
import faceIcon from "../../../Assets/Images/store/faceIcon.png";
import whatsIcon from "../../../Assets/Images/store/whatsIcon.png";
import twitterIcon from "../../../Assets/Images/store/twitterIcon.png";
import gmailIcon from "../../../Assets/Images/store/gmailIcon.png";
import instaIcon from "../../../Assets/Images/store/instaIcon.png";
import classes from "../../../styles/storesDetails.module.css";
import ReactStars from "react-rating-stars-component";
import strings from "../../../Assets/Local/Local";

function StoreInfo(props) {
  strings.setLanguage(props.lang);

  const ratingChanged = (newRating) => {
    console.log(newRating);
  };
  return (
    <div className={classes.sliderInfo}>
      <div
        className={
          classes.storeLogoBtns +
          " d-flex justify-content-between align-items-center mb-2 mt-lg-0 mt-3"
        }
      >
        <div className={classes.storeLogo}>
          <img src={storeLogo} alt="store Logo" className="img-fluid" />
        </div>
        <div className="d-flex flex-wrap">
          <div
            className={classes.storeFav + " py-1 px-2 border rounded bg-white"}
          >
            <i className="fas fa-heart"></i>
          </div>
          <div
            className={classes.following + " mx-2 py-1 border rounded text-white text-center"}
          >
            <i className={classes.followingIcon + " fas fa-star"}></i> <span className={classes.followSpan}>{strings.followBTN}</span>
            <span className={classes.followingSpan}>{strings.followingBTN}</span>
          </div>
          <div
            className={classes.chat + " py-1 border rounded text-white"}
          >
            <i className="fas fa-comment-alt"></i>{" "}
            <span>{strings.conversationBTN}</span>
          </div>
        </div>
      </div>
      <h4>محلات العويس</h4>
      <h6 className={classes.storeCat}>الكترونيات</h6>
      <span className={classes.storeRate}>
        <i className="fas fa-star"></i> 4.5
      </span>
      <div
        className={
          props.lang === "ar"
            ? "border-bottom pb-3 mt-2 mb-3 ml-sm-3 ml-0"
            : "border-bottom pb-3 mt-2 mb-3 mr-sm-3 mr-0"
        }
      >
        <div
          className={
            classes.storeLocation +
            " bg-white d-flex justify-content-between rounded-lg"
          }
        >
          <p className="mb-0 px-3 py-2 text-truncate">
            شارع عمرو بن العاص، الوشام، الرياض
          </p>
          <div className="py-2 px-3 text-white d-flex align-items-center">
            <a href="https://www.google.com" target="_blank">
              <i className="fas fa-map-marker-alt"></i>
            </a>
          </div>
        </div>
      </div>
      <div
        className={
          props.lang === "ar"
            ? "border-bottom pb-2 mb-2 ml-sm-3 ml-0"
            : "border-bottom pb-2 mb-2 mr-sm-3 mr-0"
        }
      >
        <h6 className={classes.workDates}>{strings.wordkTimes}</h6>
        <p className="text-muted mb-2">
          يوميا {strings.timeFrom} 09:00 صباحا {strings.timeTo} 11:00 مساء
        </p>
        <p className="text-muted mb-2">
          {strings.except} {strings.day} الجمعة {strings.timeFrom} 03:00 عصرا{" "}
          {strings.timeTo} 10:00 مساء
        </p>
      </div>
      <div
        className={
          classes.storeSocials +
          ` ${props.lang === "ar"
            ? " border-bottom pb-2 mb-3 ml-sm-3 ml-0"
            : " border-bottom pb-2 mb-3 mr-sm-3 mr-0"
          }`
        }
      >
        <a
          href="#"
          target="_blank"
          className={props.lang === "ar" ? "ml-2" : "mr-2"}
        >
          <img src={whatsIcon} alt="" className="img-fluid" />
        </a>
        <a
          href="#"
          target="_blank"
          className={props.lang === "ar" ? "ml-2" : "mr-2"}
        >
          <img src={gmailIcon} alt="" className="img-fluid" />
        </a>
        <a
          href="#"
          target="_blank"
          className={props.lang === "ar" ? "ml-2" : "mr-2"}
        >
          <img src={twitterIcon} alt="" className="img-fluid" />
        </a>
        <a
          href="#"
          target="_blank"
          className={props.lang === "ar" ? "ml-2" : "mr-2"}
        >
          <img src={instaIcon} alt="" className="img-fluid" />
        </a>
        <a href="#" target="_blank">
          <img src={faceIcon} alt="" className="img-fluid" />
        </a>
      </div>
      <div
        className={
          classes.storeAdRate +
          " bg-white py-3 px-4 d-flex justify-content-between align-items-center shadow-sm"
        }
      >
        <span>{strings.addYourRate}</span>
        <div className={classes.rating}>
          <ReactStars
            count={5}
            onChange={ratingChanged}
            size={22}
            activeColor="#F15A24"
            value="2"
          />
        </div>
      </div>
    </div>
  );
}

export default StoreInfo;
