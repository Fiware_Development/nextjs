import { useState } from 'react'
import classes from '../../../styles/filterShopsProducts.module.css'
import { Accordion, Card, Button, Form } from 'react-bootstrap'
import filterBTN from '../../../Assets/Images/filterBTN.png'
import strings from "../../../Assets/Local/Local";

function Filtershops(props) {
    const [blocks, setBlocks] = useState([])
    const [activites, setActivites] = useState([])
    const [categories, setCategories] = useState([])

    const handleFilterBlocks = (e) => {
        let newArrayFilter = [...blocks, e.target.id]
        if (blocks.includes(e.target.id)) {
            newArrayFilter = newArrayFilter.filter(block => block !== e.target.id);
        }
        setBlocks(newArrayFilter)
    }
    const handleFilterActivites = (e) => {
        let newArrayFilter = [...activites, e.target.id]
        if (activites.includes(e.target.id)) {
            newArrayFilter = newArrayFilter.filter(activity => activity !== e.target.id);
        }
        setActivites(newArrayFilter)
    }
    const handleFilterCategories = (e) => {
        let newArrayFilter = [...categories, e.target.id]
        if (categories.includes(e.target.id)) {
            newArrayFilter = newArrayFilter.filter(category => category !== e.target.id);
        }
        setCategories(newArrayFilter)
    }

    strings.setLanguage(props.lang);


    return (
        <div className={classes.filterBody + ` ${props.lang === 'ar' ? 'text-right' : 'text-left'}`}>
            <Accordion defaultActiveKey="0">
                <Card className={classes.filterCard}>
                    <Card.Header className={classes.filterCardHeader}>
                        <div className="d-flex justify-content-between align-items-center">
                            <Accordion.Toggle as={Button} variant="link" eventKey="0" className={classes.filterBTN}>
                                <div><span><img src={filterBTN} alt="filter BTN" className="img-fluid" /></span> <span className="mx-2">{strings.blocks}</span></div>
                            </Accordion.Toggle>
                            <span style={{ cursor: "pointer" }} className={classes.showAllFilter}>{strings.showAll}</span>
                        </div>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body className={classes.filterCardBody}>
                            <Form.Group controlId="b1" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="b1" id="b1" onChange={handleFilterBlocks} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="b2" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="b2" id="b2" onChange={handleFilterBlocks} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="b3" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="b3" id="b3" onChange={handleFilterBlocks} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="b4" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="b4" id="b4" onChange={handleFilterBlocks} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="b5" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="b5" id="b5" onChange={handleFilterBlocks} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
            <Accordion defaultActiveKey="0">
                <Card className={classes.filterCard}>
                    <Card.Header className={classes.filterCardHeader}>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0" className={classes.filterBTN}>
                            <span><img src={filterBTN} alt="filter BTN" className="img-fluid" /></span> <span className="mx-2">{strings.activities}</span>
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body className={classes.filterCardBody}>
                            <Form.Group controlId="a0" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label={strings.all} onChange={handleFilterActivites} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a1" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="a1" onChange={handleFilterActivites} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a2" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="a2" onChange={handleFilterActivites} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="a3" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="a3" onChange={handleFilterActivites} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
            <Accordion defaultActiveKey="0">
                <Card className={classes.filterCard}>
                    <Card.Header className={classes.filterCardHeader}>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0" className={classes.filterBTN}>
                            <span><img src={filterBTN} alt="filter BTN" className="img-fluid" /></span> <span className="mx-2">{strings.categories}</span>
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body className={classes.filterCardBody}>
                            <Form.Group controlId="c0" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label={strings.all} onChange={handleFilterActivites} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="c1" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="c1" onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="c2" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="c2" onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                            <Form.Group controlId="c3" className={classes.filterFormGroup}>
                                <Form.Check type="checkbox" label="c3" onChange={handleFilterCategories} className={props.lang === 'ar' ? classes.filterCheckBox + ' ' + classes.filterLabelAR : classes.filterCheckBox + ' ' + classes.filterLabelEN} />
                            </Form.Group>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    )
}

export default Filtershops
