import { useState, useEffect } from "react";
import Shopcard from '../../../components/Cards/shopcard'
import { Row, Col } from 'react-bootstrap'
import productAd from '../../../Assets/Images/productAd.png'

function Allshops(props) {

    const [allStores, setAllStores] = useState([
        {
            id: 1,
            name: "store one",
            rate: 2.5,
            price: 200,
        },
        {
            id: 2,
            name: "store two",
            rate: 4.0,
            price: 250,
        },
        {
            id: 3,
            name: "store three",
            rate: 2.5,
            price: 200,
        },
        {
            id: 4,
            name: "store four",
            rate: 2.5,
            price: 200,
        },
        {
            id: 5,
            name: "store five",
            rate: 2.5,
            price: 200,
        },
        {
            id: 6,
            name: "store six",
            rate: 2.5,
            price: 200,
        },
    ]);

    function addFollowingHandler(storeId) {
        console.log("Add Succes", storeId)
    }
    function removeFollowingHandler(storeId) {
        console.log("Removed Succes", storeId)
    }

    return (
        <div>
            <Row>
                {allStores.map((store) => {
                    return (
                        <Col lg="4" md="6" sm="6" key={store.id}>
                            <Shopcard
                                lang={props.lang}
                                storeData={store}
                                removeStoreFromFollowing={removeFollowingHandler}
                                addStoreToFollowing={addFollowingHandler}
                            />
                        </Col>
                    );
                })}
                <Col lg="12">
                    <div className="">
                        <img src={productAd} alt="product ad" className="w-100 my-4" />
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default Allshops
