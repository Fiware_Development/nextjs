import { useContext } from "react";
import classes from "../../../styles/Footer.module.css";
import { Container, Row, Col } from "react-bootstrap";
import Link from "next/link";
import logo from "../../../Assets/Images/logo.png";
import chatIcon from "../../../Assets/Images/footer/chatIcon.png";
import faceIcon from "../../../Assets/Images/footer/faceIcon.png";
import whatsIcon from "../../../Assets/Images/footer/whatsIcon.png";
import twitterIcon from "../../../Assets/Images/footer/twitterIcon.png";
import gmailIcon from "../../../Assets/Images/footer/gmailIcon.png";
import instaIcon from "../../../Assets/Images/footer/instaIcon.png";
import AppStore from "../../../Assets/Images/footer/AppStore.png";
import GooglePlay from "../../../Assets/Images/footer/GooglePlay.png";
import strings from "../../../Assets/Local/Local";
import LangContext from "../../../store/LangContext";

const Footer = () => {
  const { lang } = useContext(LangContext);

  return (
    <footer
      className={
        classes.footer +
        ` ${lang === "ar" ? "text-right pt-4 bg-white" : "text-left pt-4 bg-white"
        } `
      }
    >
      <Container fluid>
        <Row>
          <Col lg="3" md="12">
            <div className={classes.footerLogo + " pt-lg-5 pt-md-0"}>
              <Link href="/">
                <img src={logo} alt="logo" />
              </Link>
              <p className="mt-4">تطبيق يجمع لك جميع الخدمات في مكان واحد</p>
              <div
                className={
                  classes.footerStores +
                  " align-items-center d-none d-lg-flex"
                }
              >
                <a
                  href="#"
                  target="_blank"
                  className={lang === "ar" ? "ml-2" : "mr-2"}
                >
                  <img
                    src={GooglePlay}
                    alt="GooglePlay"
                    className="img-fluid"
                  />
                </a>
                <a href="#" target="_blank">
                  <img src={AppStore} alt="AppStore" className="img-fluid" />
                </a>
              </div>
            </div>
          </Col>
          <Col lg="2" md="4" sm="6">
            <div className={classes.footerLinks}>
              <h5>{strings.mostSearched}</h5>
              <h6>
                <Link href="/">الكترونيات</Link>
              </h6>
              <h6>
                <Link href="/">رجالي</Link>
              </h6>
              <h6>
                <Link href="/">الجمال و العطور</Link>
              </h6>
              <h6>
                <Link href="/">نسائي</Link>
              </h6>
              <h6>
                <Link href="/">الكترونيات</Link>
              </h6>
              <div className="d-lg-none d-block">
                <h5>{strings.intellectualProperty}</h5>
                <h6>
                  <Link href="/">{strings.tradeMarkCopyrights}</Link>
                </h6>
              </div>
            </div>
          </Col>
          <Col lg="2" md="4" sm="6">
            <div className={classes.footerLinks}>
              <h5>{strings.myAccount}</h5>
              <h6>
                <Link href="/">{strings.loginSingup}</Link>
              </h6>
              <h6>
                <Link href="/">{strings.Mypurchases}</Link>
              </h6>
              <h6>
                <Link href="/">{strings.myCart}</Link>
              </h6>
              <h6>
                <Link href="/chat">{strings.conversationBTN}</Link>
              </h6>
              <h6>
                <Link href="/">{strings.myAddress}</Link>
              </h6>
              <h6>
                <Link href="/">{strings.followMarkets}</Link>
              </h6>
              <h6>
                <Link href="/favourite">{strings.favourite}</Link>
              </h6>
              <h6>
                <Link href="/">{strings.accountSettings}</Link>
              </h6>
              <h6>
                <Link href="/">{strings.accountMovements}</Link>
              </h6>
            </div>
          </Col>
          <Col lg="2" md="4" sm="12">
            <div className={classes.footerLinks}>
              <div className="d-lg-block d-none">
                <h5>{strings.intellectualProperty}</h5>
                <h6>
                  <Link href="/">{strings.tradeMarkCopyrights}</Link>
                </h6>
              </div>
              <h5>{strings.annotations}</h5>
              <h6>
                <Link href="/subscription-user">
                  {strings.subscriptionUser}
                </Link>
              </h6>
              <h6>
                <Link href="/subscription-store">
                  {strings.subscriptionStore}
                </Link>
              </h6>
              <h6>
                <Link href="/subscription-representatives">
                  {strings.subscriptionRepresent}
                </Link>
              </h6>
              <h6>
                <Link href="/">{strings.conditionsAndRules}</Link>
              </h6>
              <h6>
                <Link href="/">{strings.howToPurchase}</Link>
              </h6>
              <h6>
                <Link href="/">{strings.commonQuestions}</Link>
              </h6>
            </div>
          </Col>
          <Col
            lg="3"
            md="12"
            className="pt-lg-5 pt-md-0"
          >
            <div className='my-lg-0 my-3'>
              <div
                className={
                  classes.footerChat +
                  " d-flex align-items-center p-3 rounded mb-3"
                }
              >
                <div className={classes.footerChatImg}>
                  <img src={chatIcon} alt="chat icon" className="img-fluid" />
                </div>
                <div
                  className={
                    classes.footerChatcontent +
                    ` ${lang === "ar" ? "mr-3" : "ml-3"}`
                  }
                >
                  <h5 className="m-0 mb-1">{strings.centerOfSupport}</h5>
                  <p className="mb-0">
                    {strings.haveAQuestion}
                    <br /> {strings.hereToHelp}
                  </p>
                </div>
              </div>
              <div className={classes.socialIcons}>
                <h5>{strings.followUs}</h5>
                <a
                  href="#"
                  target="_blank"
                  className={lang === "ar" ? "ml-2" : "mr-2"}
                >
                  <img src={whatsIcon} alt="" className="img-fluid" />
                </a>
                <a
                  href="#"
                  target="_blank"
                  className={lang === "ar" ? "ml-2" : "mr-2"}
                >
                  <img src={gmailIcon} alt="" className="img-fluid" />
                </a>
                <a
                  href="#"
                  target="_blank"
                  className={lang === "ar" ? "ml-2" : "mr-2"}
                >
                  <img src={twitterIcon} alt="" className="img-fluid" />
                </a>
                <a
                  href="#"
                  target="_blank"
                  className={lang === "ar" ? "ml-2" : "mr-2"}
                >
                  <img src={instaIcon} alt="" className="img-fluid" />
                </a>
                <a href="#" target="_blank">
                  <img src={faceIcon} alt="" className="img-fluid" />
                </a>
              </div>
            </div>
          </Col>
          <Col lg="10" md="6" sm="6" className="mx-lg-auto mx-0">
            <div
              className={
                classes.websiteLinks + " py-lg-3 py-0 mt-lg-5 mt-0"
              }
            >
              <Link href="/">
                <a className={lang === "ar" ? "ml-4" : "mr-4"}>
                  {strings.aboutSite}
                </a>
              </Link>
              <Link href="/">
                <a className={lang === "ar" ? "ml-4" : "mr-4"}>
                  {strings.jobs}
                </a>
              </Link>
              <Link href="/">
                <a className={lang === "ar" ? "ml-4" : "mr-4"}>
                  {strings.tajwalprivacy}
                </a>
              </Link>
              <Link href="/">{strings.siteAgreement}</Link>
            </div>
          </Col>
          <Col md="6" sm="6" className="d-lg-none d-block">
            <div
              className={
                classes.footerStores + ` ${lang === "ar" ? "text-left" : "text-right"}`
              }
            >
              <div className="mb-3"><a
                href="#"
                target="_blank"
              >
                <img
                  src={GooglePlay}
                  alt="GooglePlay"
                  className="img-fluid"
                />
              </a></div>
              <div>
                <a href="#" target="_blank">
                  <img src={AppStore} alt="AppStore" className="img-fluid" />
                </a>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
      <div className={classes.footerCopyright + " py-2 text-white text-center"}>
        &copy; 2021, All Rights Reserved OTS
      </div>
    </footer>
  );
};

export default Footer;
