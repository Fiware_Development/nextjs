import { useContext, useState, useEffect } from "react";
import Link from "next/link";

import { NavDropdown } from "react-bootstrap";
import strings from "../../../../Assets/Local/Local";
import LangContext from "../../../../store/LangContext";
import HeaderFilterContext from "../../../../store/HeaderFilterContext";
import GeneralPathContext from "../../../../store/GeneralPath";
import TempShareDataContext from "../../../../store/TempShareData";

import styles from "../../../../styles/Nav.module.css";
const DropdownMenu = ({ city }) => {
  const { tempRegions, tempMarkets } = useContext(TempShareDataContext);

  const { lang } = useContext(LangContext);
  const { headerFilterHandel } = useContext(HeaderFilterContext);
  const { GoTo } = useContext(GeneralPathContext);
  const [CurrentSelectedRegion, setCurrentSelectedRegion] = useState(null);

  const [districts, setDistricts] = useState(
    tempRegions.length > 0 &&
      tempRegions.filter((item) => {
        return item.cityId === city.id;
      })
  );

  const [markets, setMarkets] = useState([]);
  const [hasChild, setHasChild] = useState(false);
  const [districtIsOpen, setDistrictIsOpen] = useState(false);
  const [marketsIsOpen, setMarketsIsOpen] = useState(false);
  const cityHandel = () => {
    headerFilterHandel("city", city.id);
    headerFilterHandel("region", -1);
    headerFilterHandel("market", -1);
    headerFilterHandel("activity", []);

    setDistrictIsOpen(false);
    setMarketsIsOpen(false);
    GoTo(2, city);
  };

  const regionHandel = (region) => {
    setDistrictIsOpen(false);
    setMarketsIsOpen(false);
    headerFilterHandel("city", city.id);
    headerFilterHandel("region", region.id);
    headerFilterHandel("market", -1);
    headerFilterHandel("activity", []);

    GoTo(3, region);
  };
  useEffect(() => {
    let arr =
      CurrentSelectedRegion !== null
        ? tempMarkets.length > 0 &&
          tempMarkets.filter((item) => {
            return item.regionId === CurrentSelectedRegion;
          })
        : [];
    setMarkets(arr);
  }, [CurrentSelectedRegion]);
  strings.setLanguage(lang);
  return (
    <div className={styles.dropdown_container}>
      <div className={styles.dropdown_link}>
        <Link href="/">
          <a onClick={cityHandel}>{city.name[lang]}</a>
        </Link>
        <i
          className="fas fa-chevron-down"
          onClick={() => {
            setDistrictIsOpen((prevState) => !prevState);
            setMarketsIsOpen(false);
          }}
        ></i>
      </div>

      <div
        className={`${styles.district_dropdown} ${
          districtIsOpen ? styles.show_district : ""
        }`}
      >
        {districts.length
          ? districts.map((district, index) => (
              <div className={styles.dictrict_link_wrapper} key={index}>
                <div className={styles.region_content}>
                  <Link href="/">
                    <a onClick={() => regionHandel(district)}>
                      {district.name[lang]}
                    </a>
                  </Link>

                  {lang === "ar" ? (
                    <i
                      className="fas fa-chevron-left"
                      onClick={() => {
                        setCurrentSelectedRegion(district.id);
                        setMarketsIsOpen((prevState) => !prevState);
                      }}
                    ></i>
                  ) : (
                    <i
                      className="fas fa-chevron-right"
                      onClick={() => {
                        setCurrentSelectedRegion(district.id);
                        setMarketsIsOpen((prevState) => !prevState);
                      }}
                    ></i>
                  )}
                </div>
                {markets.length ? (
                  <div
                    className={`${
                      styles[`markets_dropdown_container_${lang}`]
                    } ${marketsIsOpen ? styles.show_markets : ""}`}
                  >
                    {markets.map((market, index) => (
                      <Link href={`/market/${market.id}`} key={index}>
                        <a className={styles.market_link}>
                          {market.name[lang]}
                        </a>
                      </Link>
                    ))}
                  </div>
                ) : null}
              </div>
            ))
          : null}
      </div>
    </div>
  );
};

export default DropdownMenu;
