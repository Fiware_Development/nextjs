import { useContext } from "react";
import Modal from "react-bootstrap/Modal";
import LangContext from "../../../../store/LangContext";
import styles from "../../../../styles/Header.module.css";
const FilterModal = ({ data, modelIsOpen, handleClose, filterHandel }) => {
  const { lang } = useContext(LangContext);
  return (
    <Modal
      show={modelIsOpen}
      onHide={handleClose}
      className={styles.header_filter_modal}
    >
      <Modal.Body className={styles.filter_body}>
        <div
          className="container"
          style={lang === "ar" ? { direction: "rtl" } : { direction: "ltr" }}
        >
          <div className="row">
            {data.map((item, index) => (
              <div
                className={`col-4 ${styles.filter_modal_items}`}
                key={index}
                onClick={() => {
                  filterHandel(item);
                  handleClose();
                }}
              >
                {item.name}
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default FilterModal;
