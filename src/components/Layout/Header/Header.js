import React, { useState, useContext, useEffect } from "react";
import Link from "next/link";
import Input from "@material-ui/core/Input";
import Dropdown from "react-bootstrap/Dropdown";
import { useRouter } from "next/router";

import ActiveLink from "../../ActiveLink/ActiveLink";
import AccountDropDown from "./AccountDropDown/AccountDropDown";
import Nav from "../Nav/Nav";
import MarketAutoSuggest from "./MarketAutoSuggest";
import FilterModal from "./FilterModal/FilterModal";
import strings from "../../../Assets/Local/Local";

import LangContext from "../../../store/LangContext";
import HeaderFilterContext from "../../../store/HeaderFilterContext";

import logo from "../../../Assets/Images/logo.png";
import arabicFlag from "../../../Assets/Images/arabicflag.png";
import EnFlag from "../../Icons/EnFlag";

import styles from "../../../styles/Header.module.css";

const Header = () => {
  const { lang, handelLang } = useContext(LangContext);
  const { filter, headerFilterHandel } = useContext(HeaderFilterContext);
  const router = useRouter();

  const [searchByType, setSearchByType] = useState();
  const [activitySelected, setActivitySelected] = useState();
  const [marketSelected, setMarketSelected] = useState();
  const [filterModalIsOpen, setFilterModalIsOpen] = useState(false);
  const [modalValue, setModalValue] = useState();
  const newLang = lang === "ar" ? "en" : "ar";

  const markets = [
    { name: "محلات المجد", id: 1 },
    { name: "نزهة رياض", id: 2 },
    { name: "محلات العويس", id: 3 },
    { name: "محلات العويس", id: 4 },
  ];
  const activityData = [
    { name: "الكترونيات", id: 1 },
    { name: "الجمال والعطور", id: 2 },
    { name: "نسائى", id: 3 },
    { name: "رجالى", id: 4 },
    { name: "الكترونيات", id: 5 },
    { name: "نسائى", id: 6 },
    { name: "الجمال والعطور", id: 7 },
    { name: "الكترونيات", id: 8 },
    { name: "الجمال والعطور", id: 9 },
    { name: "الكترونيات", id: 10 },
  ];
  strings.setLanguage(lang);

  const searchByTypeHandle = () => {
    if (searchByType) {
      router.push("/category");
    }
  };
  // modal functions
  const closeModal = () => {
    setFilterModalIsOpen(false);
  };

  const filterModalHandel = (item) => {
    console.log("activity id ", item);
    setModalValue(item.name);
    setActivitySelected(item);
  };
  // end

  // get autoComplete value
  const selectedActiviyHandel = (value) => {
    setActivitySelected(value);
  };
  const selectedMarketHandel = (value) => {
    setMarketSelected(value);
  };
  // end
  return (
    <>
      <header
        className={
          styles.header_container +
          ` ${lang === "ar" ? "headerFormControlAR" : "headerFormControlEN"}`
        }
      >
        <Link href="/">
          <a className={styles.img_wrapper}>
            <img src={logo} alt="Tajwal logo" />
          </a>
        </Link>
        <div className={`${styles.search_type}`}>
          <Input
            placeholder={strings.searchBytype}
            className={styles.search_input}
            onChange={(e) => setSearchByType(e.target.value)}
          />
          <button className={styles.search_btn} onClick={searchByTypeHandle}>
            <i className="fas fa-search"></i>
          </button>
        </div>

        <div className={styles.search_market}>
          <MarketAutoSuggest
            suggestData={markets}
            placeholder={strings.searchBymarket}
            selectedValueHandel={selectedMarketHandel}
          />
          <button
            className={styles.search_btn}
            onClick={() => {
              if (marketSelected) {
                headerFilterHandel("market", marketSelected.id);
              }
            }}
          >
            <i className="fas fa-search"></i>
          </button>
        </div>

        <div className={styles.search_activity}>
          <MarketAutoSuggest
            suggestData={activityData}
            placeholder={strings.searchByactivity}
            selectedValueHandel={selectedActiviyHandel}
            modalValue={modalValue}
          />

          <button
            className={styles.search_btn}
            onClick={() => {
              if (activitySelected) {
                headerFilterHandel("activity", [activitySelected.id]);
              }
            }}
          >
            <i className="fas fa-search"></i>
          </button>
        </div>
        <button
          className={styles.filer_btn}
          onClick={() => setFilterModalIsOpen(true)}
        >
          <i className="fas fa-filter"></i>
        </button>
        <div className={styles.user_wrapper}>
          <div className={`form-group ${styles.select_container}`}>
            <select
              className="form-control"
              defaultValue={
                filter.find((item) => item.name === "market_Type").id
              }
              onChange={(e) =>
                headerFilterHandel("market_Type", +e.target.value)
              }
            >
              <option value="0">{strings.lump}</option>
              <option value="1">{strings.sectoral}</option>
            </select>
          </div>
          <div className={styles.fav_noti_container}>
            <ActiveLink activeClassName="active_Link" href="/favourite">
              <a>
                <i className="fas fa-heart"></i>
              </a>
            </ActiveLink>

            <ActiveLink activeClassName="active_Link" href="/notifications">
              <a>
                <i className="fas fa-bell"></i>
                <span className={styles[`fav_count_${lang}`]}>4</span>
              </a>
            </ActiveLink>
          </div>
          <Dropdown>
            <Dropdown.Toggle variant="Secondary" className={styles.my_account}>
              <i className="fas fa-user"></i>
              <p>
                <span>Mahmoud Muhamed</span>
                <br />
                {strings.myAccount}
              </p>
            </Dropdown.Toggle>
            <AccountDropDown />
          </Dropdown>
          <button
            className={styles.lang_btn}
            onClick={() => handelLang(newLang)}
          >
            {lang === "en" ? <img src={arabicFlag} alt="arabic" /> : <EnFlag />}
          </button>
        </div>
        <div className={styles.user_actions_responsive}>
          <ActiveLink activeClassName="active_Link" href="/favourite">
            <a className={styles.favourite_page}>
              <i className="fas fa-heart"></i>
            </a>
          </ActiveLink>

          <ActiveLink activeClassName="active_Link" href="/notifications">
            <a className={styles.notifications_page}>
              <i className="fas fa-bell"></i>
              <span className={styles[`fav_count_${lang}`]}>4</span>
            </a>
          </ActiveLink>
          <button
            className={styles.lang_btn}
            onClick={() => handelLang(newLang)}
          >
            {lang === "en" ? <img src={arabicFlag} alt="arabic" /> : <EnFlag />}
          </button>
          <Dropdown>
            <Dropdown.Toggle variant="Secondary" className={styles.my_account}>
              <i className="fas fa-bars"></i>
            </Dropdown.Toggle>
            <AccountDropDown />
          </Dropdown>
        </div>
        <FilterModal
          data={activityData}
          handleClose={closeModal}
          filterHandel={filterModalHandel}
          modelIsOpen={filterModalIsOpen}
        />
      </header>
      <Nav />
    </>
  );
};

export default Header;
