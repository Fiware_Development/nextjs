import React, { useState, useEffect } from "react";
import Autosuggest from "react-autosuggest";
const MarketAutoSuggest = ({
  suggestData,
  placeholder,
  selectedValueHandel,
  modalValue,
}) => {
  const [suggestValue, setSuggestValue] = useState("");
  const [suggestions, setSuggestions] = useState([]);
  useEffect(() => {
    console.log("suggest Value", suggestValue);
  }, [suggestValue]);
  useEffect(() => {
    if (modalValue) {
      setSuggestValue(modalValue);
    }
  }, [modalValue]);
  const getSuggestionValue = (suggestion) => {
    return suggestion.name;
  };

  const renderSuggestion = (suggestion) => {
    return <div className="suggest_value">{suggestion.name}</div>;
  };

  const getSuggestions = (value) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0
      ? []
      : suggestData.filter(
          (item) => item.name.toLowerCase().slice(0, inputLength) === inputValue
        );
  };

  const onChange = (e, { newValue }) => {
    setSuggestValue(newValue);
  };

  const onSuggestionsFetchRequested = ({ value }) => {
    setSuggestions(getSuggestions(value));
  };

  const onSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  const suggestSelected = (event, { suggestion, suggestionValue }) => {
    selectedValueHandel(suggestion);
  };
  return (
    <Autosuggest
      suggestions={suggestions}
      onSuggestionsFetchRequested={onSuggestionsFetchRequested}
      onSuggestionsClearRequested={onSuggestionsClearRequested}
      getSuggestionValue={getSuggestionValue}
      renderSuggestion={renderSuggestion}
      onSuggestionSelected={suggestSelected}
      inputProps={{
        placeholder: placeholder,
        value: suggestValue,
        onChange: onChange,
      }}
    />
  );
};

export default MarketAutoSuggest;
