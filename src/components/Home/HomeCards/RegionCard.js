import React, { useContext, useState ,useEffect} from "react";
import { Col, Container, Row } from "react-bootstrap";
import pinImg from "../../../Assets/Images/pin.png";
import LangContext from "../../../store/LangContext";
import GeneralPathContext from "../../../store/GeneralPath";
import TempShareDataContext from "../../../store/TempShareData";

import styles from "../../../styles/Home.module.css";

export default function RegionCard(props) {
  const { GeneralPath, handleGeneralPath } = useContext(GeneralPathContext);
  const { lang } = useContext(LangContext);
  const { tempRegions } = useContext(TempShareDataContext);

  const [regions, setregions] = useState([]);

  useEffect(() => {
    let NewRegions =
      tempRegions.length > 0 &&
      tempRegions.filter((item) => {
        return item.cityId === props.cityId;
      });
    setregions(NewRegions);
  }, [props]);

  console.log("region props===> ", props);
  return (
    <Container>
      <Row>
        {regions.length > 0 &&
          regions.map((Region, index) => {
            return (
              <Col lg="2" md="3" sm="3" xs="6" className="mb-3" key={index}>
                <div
                  className={styles.cityCard}
                  onClick={() => {
                    handleGeneralPath(Region, "region");
                  }}
                >
                  <div className={styles.imgPinDiv}>
                    <img src={pinImg} />
                  </div>
                  <h6 className={styles.text}>{Region.name[lang]}</h6>
                </div>
              </Col>
            );
          })}
      </Row>
    </Container>
  );
}
