import React, { useContext, useState, useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";
import market1 from "../../../Assets/Images/market1.png";
import market2 from "../../../Assets/Images/market2.png";
import market3 from "../../../Assets/Images/market3.png";
import market4 from "../../../Assets/Images/market4.png";
import market5 from "../../../Assets/Images/market5.png";
import market6 from "../../../Assets/Images/market6.png";

import LangContext from "../../../store/LangContext";
import GeneralPathContext from "../../../store/GeneralPath";
import TempShareDataContext from "../../../store/TempShareData";

import styles from "../../../styles/Home.module.css";
import Link from "next/link";

export default function MarketCard(props) {
  const { GeneralPath, handleGeneralPath } = useContext(GeneralPathContext);
  const { lang } = useContext(LangContext);
  const { tempMarkets } = useContext(TempShareDataContext);
  const [markets, setmarkets] = useState([]);

  useEffect(() => {
    console.log("hello from market card");
    prepareData();
  }, [props]);

  const prepareData = () => {
    let currentMarkets = [];

    console.log("prepare", tempMarkets);
    console.log("market props", props);
    //handle All activities case with specific region
    if (
      props.regionId !== -1 &&
      props.activityIds &&
      props.activityIds.length > 0 &&
      props.activityIds.includes(0)
    ) {
      currentMarkets = tempMarkets.filter((item) => {
        return item.regionId === props.regionId;
      });
    }
    //handle All case
    else if (
      props.activityIds &&
      props.activityIds.length > 0 &&
      props.activityIds.includes(0)
    ) {
      console.log("case All", tempMarkets);
      currentMarkets = tempMarkets;
    }
    //handle case (filter by region and activity together)
    else if (
      props.regionId !== -1 &&
      props.activityIds &&
      props.activityIds.length > 0
    ) {
      currentMarkets = tempMarkets.filter((item) => {
        return (
          props.activityIds.includes(item.activityId) &&
          item.regionId === props.regionId
        );
      });
    }
    // handle case (filter by activity only from header)
    else if (props.activityIds && props.activityIds.length > 0) {
      currentMarkets = tempMarkets.filter((item) => {
        return props.activityIds.includes(item.activityId);
      });
    }
    // handle case (filter by region only from sub header)
    else if (props.regionId !== -1) {
      currentMarkets = tempMarkets.filter((item) => {
        return item.regionId === props.regionId;
      });
    } else {
      currentMarkets = tempMarkets;
    }

    setmarkets(currentMarkets);
  };

  const allMarkets = [
    { image: market1, name: "اسواق طيبة", id: 1 },
    { image: market2, name: "سوق الزل والمشالح", id: 1 },
    { image: market3, name: "أسواق العويس", id: 1 },
    { image: market4, name: "أسواق المجد", id: 1 },
    { image: market5, name: "أسواق نزهة الرياض", id: 1 },
    { image: market6, name: "City stars", id: 1 },
  ];

  return (
    <Container>
      <Row>
        {markets.length > 0 &&
          markets.map((market, index) => {
            return (
              <Col lg="3" md="4" sm="6" xs="12" className="mb-3" key={index}>
                <Link href="/stores">
                  <div
                    className={styles.marketCard}
                    onClick={() => {
                      handleGeneralPath(market.id, "market");
                    }}
                  >
                    <div className={styles.imgPinDiv}>
                      <img src={market.image} />
                    </div>
                    <h6 className={styles.text}>{market.name[lang]}</h6>
                  </div>
                </Link>
              </Col>
            );
          })}
      </Row>
    </Container>
  );
}
