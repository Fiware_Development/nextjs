import React, { useContext } from "react";
import { Col, Container, Row } from "react-bootstrap";
import pinImg from "../../../Assets/Images/pin.png";
import GeneralPathContext from "../../../store/GeneralPath";
import TempShareDataContext from "../../../store/TempShareData";
import LangContext from "../../../store/LangContext";

import styles from "../../../styles/Home.module.css";

export default function CityCard() {
  const { GeneralPath, handleGeneralPath } = useContext(GeneralPathContext);
  const { tempCities } = useContext(TempShareDataContext);
  const { lang } = useContext(LangContext);

  const allcities = [
    { name: "الرياض", id: 1 },
    { name: "المدينة المنورة", id: 1 },
    { name: "مكة المكرمة", id: 1 },
    { name: "الطائف", id: 1 },
    { name: "جدة", id: 1 },
    { name: "ابها", id: 1 },
    { name: "الدمام", id: 1 },
    { name: "بريدة", id: 1 },
    { name: "تبوك", id: 1 },
    { name: "القطيف", id: 1 },
    { name: "خميس مشيط", id: 1 },
    { name: "الخبر", id: 1 },
    { name: "الرياض", id: 1 },
    { name: "المدينة المنورة", id: 1 },
    { name: "مكة المكرمة", id: 1 },
    { name: "الطائف", id: 1 },
    { name: "جدة", id: 1 },
    { name: "ابها", id: 1 },
    { name: "الدمام", id: 1 },
    { name: "بريدة", id: 1 },
    { name: "تبوك", id: 1 },
    { name: "القطيف", id: 1 },
    { name: "خميس مشيط", id: 1 },
    { name: "الخبر", id: 1 },
  ];

  return (
    <Container>
      <Row>
        {tempCities.map((city, index) => {
          return (
            <Col lg="2" md="3" sm="3" xs="6" className="mb-3" key={index}>
              <div
                className={styles.cityCard}
                onClick={() => {
                  handleGeneralPath(city, "city");
                }}
              >
                <div className={styles.imgPinDiv}>
                  <img src={pinImg} />
                </div>
                <h6 className={styles.text}>{city.name[lang]}</h6>
              </div>
            </Col>
          );
        })}
      </Row>
    </Container>
  );
}
