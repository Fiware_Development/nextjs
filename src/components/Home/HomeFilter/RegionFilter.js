import React, { useContext, useState, useEffect } from "react";
import "antd/dist/antd.css";
import { Collapse, Checkbox, Radio } from "antd";
import LangContext from "../../../store/LangContext";
import GeneralPathContext from "../../../store/GeneralPath";
import HeaderFilterContext from "../../../store/HeaderFilterContext";
import String from "../../../Assets/Local/Local";

import styles from "../../../styles/Home.filter.module.css";

const RegionFilter = () => {
  const { Panel } = Collapse;
  const { lang } = useContext(LangContext);
  const { GoTo } = useContext(GeneralPathContext);
  const { filter, headerFilterHandel } = useContext(HeaderFilterContext);
  const [activityFilterValue, setActivityFilterValue] = useState(null);

  const activitiesData = [
    { name: { ar: "كل الانشطة", en: "All Activities" }, id: 0 },
    { name: { ar: "الكترونيات", en: "electronics" }, id: 1 },
    { name: { ar: "الجمال والعطور", en: "Beauty" }, id: 2 },
    { name: { ar: "نسائى", en: "women" }, id: 3 },
  ];

  useEffect(() => {
    // initalize value and activity
    
    let activityPrevValue = filter.filter((item, ind) => {
      return item.name === "activity" && item.id.length > 0;
    });

    if (activityPrevValue.length > 0 && activityPrevValue[0].id.length > 0) {
      setActivityFilterValue(activityPrevValue[0].id[0]);
    }
  }, [filter]);

  function onChange(activity) {
    console.log(activity.target.value);
    setActivityFilterValue(activity.target.value);
    let currentfilter = filter.filter((item, index) => {
      if (item.name === "activity") {
        item.id = [activity.target.value];
        return item;
      }
      return item;
    });
    let choosenActivity = activitiesData.filter((act, indx) => {
      return act.id === activity.target.value;
    });
    GoTo(3, choosenActivity[0], "activityFilter");
    headerFilterHandel(currentfilter);
  }

  return (
    <div className={lang === "ar" && styles.filterAr}>
      <Collapse
        defaultActiveKey={["1"]}
        style={{ textAlign: lang === "ar" ? "right" : "left" }}
      >
        <Panel header={String.activityfiltertitleregion} key="1">
          <Radio.Group onChange={onChange} value={activityFilterValue}>
            {activitiesData.map((activity, index) => {
              return (
                <p
                  className={
                    lang === "en" ? styles.filterAttrEn : styles.filterAttrAr
                  }
                  key={index}
                >
                  <Radio value={activity.id}> {activity.name[lang]}</Radio>
                </p>
              );
            })}
          </Radio.Group>
        </Panel>
      </Collapse>
    </div>
  );
};

export default RegionFilter;
