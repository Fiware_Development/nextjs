import { useState, useEffect } from "react";
import Shopcard from "../Cards/shopcard";
import { Row, Col } from "react-bootstrap";

function Favstores(props) {
  const [allStores, setAllStores] = useState([
    {
      id: 1,
      name: "store one",
      rate: 2.5,
      price: 200,
    },
    {
      id: 2,
      name: "store two",
      rate: 4.0,
      price: 250,
    },
    {
      id: 3,
      name: "store three",
      rate: 2.5,
      price: 200,
    },
    {
      id: 4,
      name: "store four",
      rate: 2.5,
      price: 200,
    },
    {
      id: 5,
      name: "store five",
      rate: 2.5,
      price: 200,
    },
    {
      id: 6,
      name: "store six",
      rate: 2.5,
      price: 200,
    },
  ]);

  function removeStore(storeId) {
    return allStores.filter((store) => store.id !== storeId);
  }

  function removeFavoriteHandler(storeId) {
    console.log("Removed Succes", storeId);
    setAllStores(removeStore(storeId));
  }

  useEffect(() => {}, [allStores]);

  return (
    <Row>
      {allStores.map((store) => {
        return (
          <Col lg="4" md="6" sm="6" key={store.id}>
            <Shopcard
              lang={props.lang}
              favStoreState={true}
              storeData={store}
              removeStoreFromFavorite={removeFavoriteHandler}
            />
          </Col>
        );
      })}
    </Row>
  );
}

export default Favstores;
