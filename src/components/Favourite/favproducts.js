import { useEffect, useState } from "react";
import Productcard from "../Cards/productcard";
import { Row, Col } from "react-bootstrap";

function Favproucts(props) {
  const [allProducts, setAllProducts] = useState([
    {
      id: 1,
      name: "product one",
      rate: 2.5,
      price: 200,
    },
    {
      id: 2,
      name: "product two",
      rate: 4.0,
      price: 250,
    },
    {
      id: 3,
      name: "product three",
      rate: 2.5,
      price: 200,
    },
    {
      id: 4,
      name: "product four",
      rate: 2.5,
      price: 200,
    },
    {
      id: 5,
      name: "product five",
      rate: 2.5,
      price: 200,
    },
    {
      id: 6,
      name: "product six",
      rate: 2.5,
      price: 200,
    },
  ]);

  function removeProduct(prodId) {
    return allProducts.filter((prod) => prod.id !== prodId);
  }

  function removeFavoriteHandler(prodId) {
    console.log("Removed Succes", prodId);
    setAllProducts(removeProduct(prodId));
    console.log("Favorite Product: ", removeProduct(prodId));
  }

  // useEffect(() => {}, [allProducts]);

  return (
    <Row>
      {allProducts.map((prod) => {
        return (
          <Col xl="3" lg="4" md="6" sm="6" key={prod.id}>
            <Productcard
              lang={props.lang}
              prodData={prod}
              favState={true}
              removeProductFromFavorite={removeFavoriteHandler}
            />
          </Col>
        );
      })}
    </Row>
  );
}

export default Favproucts;
