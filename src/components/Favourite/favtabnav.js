import Favproucts from "./favproducts";
import Favstores from "./favstores";
import { Tabs, Tab } from "react-bootstrap";
import strings from "../../Assets/Local/Local";

function Favtabnav(props) {
  strings.setLanguage(props.lang);

  return (
    <div className="productDetailsTabNav favoriteTabnav">
      <Tabs
        defaultActiveKey="home"
        id="uncontrolled-tab-example"
        className="mb-3 border-0"
      >
        <Tab eventKey="home" title={strings.products}>
          <Favproucts lang={props.lang} />
        </Tab>
        <Tab eventKey="profile" title={strings.stores}>
          <Favstores lang={props.lang} />
        </Tab>
      </Tabs>
    </div>
  );
}

export default Favtabnav;
