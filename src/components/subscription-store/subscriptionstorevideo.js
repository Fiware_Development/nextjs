import classes from '../../styles/subscrptions.module.css'
import strings from "../../Assets/Local/Local";
import subscriptionAd from '../../Assets/Images/subscriptionAd.png'

function subscriptionvideo() {
    return (
        <div className={classes.subscriptionVideo}>
            <div>Video</div>
            <div>
                <img src={subscriptionAd} alt="subscription ad" className="img-fluid mt-4" />
            </div>
        </div>
    )
}

export default subscriptionvideo
