import { useContext } from "react";

import LangCtx from "../../store/LangContext";
import strings from "../../Assets/Local/Local";

import styles from "../../styles/shoppingCart.module.css";
const ShoppingCart = ({ product, deleteProductHandel, countHandel }) => {
  const { lang } = useContext(LangCtx);
  strings.setLanguage(lang);
  return (
    <section className={styles.product_container}>
      <div className={styles.img_container}>
        <img src={product.image} alt={product.productName} />
        <div className={styles.count}>
          <i
            className="fas fa-plus"
            onClick={() => countHandel(product.id, "add")}
          ></i>
          <span>{product.count}</span>
          <i
            className="fas fa-minus"
            onClick={() => countHandel(product.id, "remove")}
          ></i>
        </div>
      </div>
      <div className={`${styles.product_content}`}>
        <h4>{product.productName}</h4>
        <p>{`${product.price}  ${strings.currency}`}</p>
        <p>{`${strings.productState} : ${product.status}`}</p>
        <h6>{`${strings.theSeller} : ${product.seller}`}</h6>
        <i
          className={`fas fa-trash ${styles[`delete_icon_${lang}`]}`}
          onClick={() => deleteProductHandel(product.id)}
        ></i>
      </div>
    </section>
  );
};

export default ShoppingCart;
