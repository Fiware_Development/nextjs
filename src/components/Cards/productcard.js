import { useContext } from "react";
import Link from "next/link";
import classes from "../../styles/products.module.css";
import storeLogo from "../../Assets/Images/store/storeLogo.png";
import { Button } from "react-bootstrap";
import strings from "../../Assets/Local/Local";
import FavoriteProductsContext from "../../store/FavoriteProductContext";

function Productcard(props) {
  const favoriteCtx = useContext(FavoriteProductsContext);
  const productIsFavorite = favoriteCtx.productIsFavorite(props.prodData.id);

  function toggleProductFavorite() {
    if (productIsFavorite) {
      favoriteCtx.removeFavoriteProduct(props.prodData.id);
      props.removeProductFromFavorite(props.prodData.id);
    } else {
      favoriteCtx.addFavoriteProduct(props.prodData);
      props.favState === true
        ? ""
        : props.addProductToFavorite(props.prodData.id);
    }
  }

  strings.setLanguage(props.lang);

  return (
    <div
      className={
        classes.productcard + " mb-4 py-3 px-2 bg-white shadow-sm rounded"
      }
    >
      <div className={classes.productBtns + " position-relative"}>
        {props.favState ? (
          <Button
            onClick={toggleProductFavorite}
            className={
              classes.productFav +
              " position-absolute border d-lg-none d-block "
            }
            style={{
              color: productIsFavorite ? "#9EA4AF" : "#F15A24",
              top: "-10px",
              left: props.lang === "ar" ? "0" : "",
              right: props.lang === "en" ? "0" : "",
              zIndex: "50",
            }}
          >
            <i className="fas fa-heart"></i>
          </Button>
        ) : (
          <Button
            onClick={toggleProductFavorite}
            className={
              classes.productFav +
              " position-absolute border d-lg-none d-block "
            }
            style={{
              color: productIsFavorite ? "#F15A24" : "#9EA4AF",
              top: "-10px",
              left: props.lang === "ar" ? "0" : "",
              right: props.lang === "en" ? "0" : "",
              zIndex: "50",
            }}
          >
            <i className="fas fa-heart"></i>
          </Button>
        )}
      </div>
      <Link href="/products/1">
        <div
          className={props.lang === "ar" ? "text-right" : "text-left"}
          style={{ cursor: "pointer" }}
        >
          <div
            className={
              classes.productImg +
              " d-flex justify-content-center align-items-center mx-auto mb-2 pt-lg-0 pt-4"
            }
          >
            <img src={storeLogo} alt="product img" className="img-fluid" />
          </div>
          <h4 className="mb-2">
            {props.prodData.name}
            {/* سماعة رأس ايربودز ماكس بخاصية
                        إلغاء الضوضاء النشط من ابل
                        سماعة رأس ايربودز ماكس بخاصية
                        إلغاء الضوضاء النشط من ابل */}
          </h4>
          <span>
            <i className="fas fa-star"></i> 4.5
          </span>
          <p className={classes.productPrice + " my-2"}>
            2.000 <span className="mx-2">{strings.currency}</span>
          </p>
        </div>
      </Link>
      <div className={classes.productBtns + " d-flex justify-content-between"}>
        <Button className="w-100">{strings.addToCartBTN}</Button>
        {props.favState ? (
          <Button
            onClick={toggleProductFavorite}
            className={classes.productFav + " mx-2 border d-lg-block d-none"}
            style={{ color: productIsFavorite ? "#9EA4AF" : "#F15A24" }}
          >
            <i className="fas fa-heart"></i>
          </Button>
        ) : (
          <Button
            onClick={toggleProductFavorite}
            className={classes.productFav + " mx-2 border d-lg-block d-none"}
            style={{ color: productIsFavorite ? "#F15A24" : "#9EA4AF" }}
          >
            <i className="fas fa-heart"></i>
          </Button>
        )}
      </div>
    </div>
  );
}

export default Productcard;
