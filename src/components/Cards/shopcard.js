import { useContext } from "react";
import Link from "next/link";
import classes from "../../styles/shops.module.css";
import storeLogo from "../../Assets/Images/store/storeLogo.png";
import strings from "../../Assets/Local/Local";
import FavoriteStoreContext from "../../store/FavoriteStoreContext";
import FollowingStoreContext from "../../store/FollowingStoreContext";

function Shopcard(props) {
  // Favorite Stores
  const favoriteCtx = useContext(FavoriteStoreContext);
  const storeIsFavorite = favoriteCtx.storeIsFavorite(props.storeData.id);

  // Following Stores
  const followCtx = useContext(FollowingStoreContext);
  const storeIsFollowing = followCtx.storeIsFollowing(props.storeData.id);

  function toggleStoreFavorite() {
    if (storeIsFavorite) {
      console.log("Removed store");
      favoriteCtx.removeFavoriteStore(props.storeData.id);
      props.removeStoreFromFavorite(props.storeData.id);
    } else {
      console.log("Added store");
      favoriteCtx.addFavoriteStore(props.storeData);
      // props.favStoreState === true ? "" : props.addStoreToFavorite(props.storeData.id)
    }
  }

  function toggleStoreFollowing() {
    if (storeIsFollowing) {
      console.log("Removed store");
      followCtx.removeFollowingStore(props.storeData.id);
      props.removeStoreFromFollowing(props.storeData.id);
    } else {
      console.log("Added store");
      followCtx.addFollowingStore(props.storeData);
      props.followStoreState ? "" : props.addStoreToFollowing(props.storeData.id);
    }
  }

  strings.setLanguage(props.lang);

  return (
    <div
      className={
        classes.shopCard +
        " d-flex justify-content-between align-items-center mb-4 py-3 px-2 bg-white shadow-sm rounded"
      }
    >
      <Link href="/stores/1">
        <div className="d-flex align-items-center">
          <div className={classes.logoShop}>
            <img src={storeLogo} alt="logo shop" className="img-fluid" />
          </div>
          <div
            className={
              classes.nameShop +
              ` ${props.lang == "ar" ? "mr-2 text-right" : "ml-2 text-left"}`
            }
          >
            <h5>محلات المجد</h5>
            <h6>الكترونيات</h6>
            <span>
              <i className="fas fa-star"></i> 4.5
            </span>
          </div>
        </div>
      </Link>
      {props.favStoreState ? (
        <div
          onClick={toggleStoreFavorite}
          style={{
            backgroundColor: "#E7ECF6",
            color: storeIsFavorite ? "#9EA4AF" : "#F15A24",
          }}
          className={classes.followBtn + " px-2 py-1 border-0 rounded"}
        >
          <i className="fas fa-heart"></i>
        </div>
      ) : (
        <div>
          <div
            onClick={toggleStoreFollowing}
            className={
              `${storeIsFollowing ? classes.followingBtn : classes.followBtn}` +
              ` ${storeIsFollowing
                ? "px-2 py-1 rounded "
                : " px-2 py-1 rounded text-white "
              } `
            }
          >
            <i className="fas fa-star"></i>{" "}
            <span>
              {storeIsFollowing ? strings.followingBTN : strings.followBTN}
            </span>
          </div>
        </div>
      )}
    </div>
  );
}

export default Shopcard;
