import { createContext, useState } from "react";

const FavoriteStoreContext = createContext({
  favorites: [],
  addFavoriteStore: (favoriteStore) => {},
  removeFavoriteStore: (StoreId) => {},
  storeIsFavorite: (StoreId) => {},
});

export function FavoriteStoreProvider(props) {
  const [favoriteStores, setFavoriteStores] = useState([]);

  const addFavoriteStore = (favoriteProdcut) => {
    setFavoriteStores((prevFavoriteStores) => {
      return prevFavoriteStores.concat(favoriteProdcut);
    });
  };

  function removeFavoriteStore(prodcutId) {
    setFavoriteStores((prevFavoriteStores) => {
      return prevFavoriteStores.filter((prod) => prod.id !== prodcutId);
    });
  }

  function storeIsFavorite(prodcutId) {
    return favoriteStores.some((prod) => prod.id === prodcutId);
  }

  const contextStore = {
    favorites: favoriteStores,
    addFavoriteStore: addFavoriteStore,
    removeFavoriteStore: removeFavoriteStore,
    storeIsFavorite: storeIsFavorite,
  };

  return (
    <FavoriteStoreContext.Provider value={contextStore}>
      {props.children}
    </FavoriteStoreContext.Provider>
  );
}

export default FavoriteStoreContext;
