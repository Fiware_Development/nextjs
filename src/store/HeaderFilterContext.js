import { createContext, useState, useEffect } from "react";

const HeaderFilterContext = createContext();

export const HeaderFilterContextProvider = (props) => {
  const defaultValue = [
    { name: "city", id: -1 },
    { name: "region", id: -1 },
    { name: "market", id: -1 },
    { name: "activity", id: [] },
    { name: "market_Type", id: 0 },
  ];

  const [filterValue, setFilterValue] = useState(defaultValue);

  useEffect(() => {
    const filterData = window.sessionStorage.getItem("tajawal_headerFilter")
      ? JSON.parse(window.sessionStorage.getItem("tajawal_headerFilter"))
      : defaultValue;

    setFilterValue(filterData);
  }, []);

  useEffect(() => {
    console.log("filterValue", filterValue);
  }, [filterValue]);

  const filterHandel = (name, id) => {
    const updatedData = filterValue.map((item) => {
      if (item.name === name) {
        item.id = id;
        return item;
      }
      return item;
    });
    
    setFilterValue(updatedData);
    window.sessionStorage.setItem(
      "tajawal_headerFilter",
      JSON.stringify(updatedData)
    );
  };

  const clearSpecificFilter = (index, clearValue) => {
    let values = filterValue.filter((item, Idx) => {
      if (index === Idx) item.id = clearValue;
      return item;
    });
    setFilterValue(values);
  };

  const clearFilter = () => {
    setFilterValue(defaultValue);
    window.sessionStorage.setItem(
      "tajawal_headerFilter",
      JSON.stringify(defaultValue)
    );
  };
  
  return (
    <HeaderFilterContext.Provider
      value={{
        filter: filterValue,
        headerFilterHandel: filterHandel,
        clearFilter: clearFilter,
        clearSpecificFilter: clearSpecificFilter,
      }}
    >
      {props.children}
    </HeaderFilterContext.Provider>
  );
};

export default HeaderFilterContext;
