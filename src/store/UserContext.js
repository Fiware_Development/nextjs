import { createContext, useState } from "react";

const UserContext = createContext({ user: {}, token: {} });

export const UserContextProvider = ({ children }) => {
  const [user, setUser] = useState({});
  const [token, setToken] = useState({});

  const userHandle = (userData, userToken) => {
    setUser(userData);
    setToken(userToken);
  };
  return (
    <UserContext.Provider value={{ user, token, userHandle }}>
      {children}
    </UserContext.Provider>
  );
};
export default UserContext;
