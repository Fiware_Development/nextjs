import { createContext, useState } from "react";

const FollowingStoreContext = createContext({
  followings: [],
  addFollowingStore: (followingStore) => {},
  removeFollowingStore: (StoreId) => {},
  storeIsFollowing: (StoreId) => {},
});

export function FollowingStoreProvider(props) {
  const [followingStores, setFollowingStores] = useState([]);

  const addFollowingStore = (followingProdcut) => {
    setFollowingStores((prevFollowingStores) => {
      return prevFollowingStores.concat(followingProdcut);
    });
  };

  function removeFollowingStore(prodcutId) {
    setFollowingStores((prevFollowingStores) => {
      return prevFollowingStores.filter((prod) => prod.id !== prodcutId);
    });
  }

  function storeIsFollowing(prodcutId) {
    return followingStores.some((prod) => prod.id === prodcutId);
  }

  const contextStore = {
    followings: followingStores,
    addFollowingStore: addFollowingStore,
    removeFollowingStore: removeFollowingStore,
    storeIsFollowing: storeIsFollowing,
  };

  return (
    <FollowingStoreContext.Provider value={contextStore}>
      {props.children}
    </FollowingStoreContext.Provider>
  );
}

export default FollowingStoreContext;
