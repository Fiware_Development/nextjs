import React, { createContext, useState } from "react";
import market1 from "../Assets/Images/market1.png";
import market2 from "../Assets/Images/market2.png";
import market3 from "../Assets/Images/market3.png";
import market4 from "../Assets/Images/market4.png";

const TempShareDataContext = createContext([]);

export const TempShareData = (props) => {
  const [tempCities, setTempCities] = useState([
    {
      name: { ar: "الرياض", en: "el rayad" },
      id: 1,
    },
    {
      name: { ar: "جدة", en: "gadaa" },
      id: 2,
    },
  ]);

  const [tempRegions, setTempRegions] = useState([
    {
      name: { ar: "حي التعاون", en: "el taawan" },
      id: 1,
      cityId: 1,
    },
    {
      name: { ar: "حي الملك فهد", en: "king fahd" },
      id: 2,
      cityId: 2,
    },
    {
      name: { ar: "الجزيرة", en: "Al Gazeera" },
      id: 3,
      cityId: 1,
    },
  ]);

  const [tempMarkets, setTempMarkets] = useState([
    {
      name: { ar: "أسواق المجد 1", en: "Al magd markets 1" },
      id: 1,
      cityId: 1,
      regionId: 1,
      activityId: 1,
      image: market1,
    },
    {
      name: { ar: "أسواق المجد 2", en: "Al magd markets 2" },
      id: 2,
      cityId: 2,
      regionId: 2,
      activityId: 2,
      image: market2,
    },
    {
      name: { ar: "أسواق المجد 3", en: "Al magd markets 3" },
      id: 3,
      cityId: 1,
      regionId: 3,
      activityId: 3,
      image: market3,
    },
    {
      name: { ar: "أسواق المجد 4", en: "Al magd markets 4" },
      id: 1,
      cityId: 1,
      regionId: 1,
      activityId: 2,
      image: market4,
    },
    

  ]);

  return (
    <TempShareDataContext.Provider
      value={{
        tempCities: tempCities,
        tempRegions: tempRegions,
        tempMarkets: tempMarkets,
      }}
    >
      {props.children}
    </TempShareDataContext.Provider>
  );
};

export default TempShareDataContext;
