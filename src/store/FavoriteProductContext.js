import { createContext, useState } from "react";

const FavoriteProductsContext = createContext({
    favorites: [],
    addFavoriteProduct: (favoriteProdcut) => { },
    removeFavoriteProduct: (ProdcutId) => { },
    productIsFavorite: (ProdcutId) => { },
});

export function FavoriteProductProvider(props) {

    const [favoriteProducts, setFavoriteProducts] = useState([])

    const addFavoriteProduct = (favoriteProdcut) => {
        setFavoriteProducts((prevFavoriteProducts) => {
            return prevFavoriteProducts.concat(favoriteProdcut);
        });
    }

    function removeFavoriteProduct(prodcutId) {
        setFavoriteProducts(prevFavoriteProducts => {
            return prevFavoriteProducts.filter(prod => prod.id !== prodcutId);
        });
    }

    function productIsFavorite(prodcutId){
        return favoriteProducts.some(prod => prod.id === prodcutId);
    }

    const contextProducts = {
        favorites: favoriteProducts,
        addFavoriteProduct: addFavoriteProduct,
        removeFavoriteProduct: removeFavoriteProduct,
        productIsFavorite: productIsFavorite,
    };

    return (
        <FavoriteProductsContext.Provider value={contextProducts}>
            {props.children}
        </FavoriteProductsContext.Provider>
    )
}

export default FavoriteProductsContext
